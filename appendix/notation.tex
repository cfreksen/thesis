\chapter{Notation and Conventions}
\label{cha:app:notation-conventions}

The purpose of this appendix is to precisely describe the parts of
notation used in this thesis that I guess the reader could be
unfamiliar with or have a different interpretation of. Just as I
assume that the reader is able to read and understand English (apart
from those readers only who are interested in the
\hyperref[sec:front:resume]{``Resum\'e''}), I assume the reader is
familiar with some parts of mathematics: Mostly parts of linear
algebra, probability theory, and algorithmics.

\begin{itemize}
\item $\Nzero$ denotes the natural numbers including $0$, i.e.,
  $\Nzero \eqdef \{0, 1, 2, \dotsc \}$, and $\Npos$ denotes the
  natural numbers excluding $0$, i.e.,
  $\Npos \eqdef \Nzero \setminus \{0\} = \{1, 2, \dotsc\}$.
\item $\R$ denotes the set of real numbers, and $\Rpos \subset \R$ is
  the set of positive real numbers, i.e.,
  $\Rpos \eqdef \{a \in \R \mid a > 0\}$.
\item For a natural number $n \in \Npos$, $[n]$ denotes the set
  $\{1, \dotsc, n\}$.
\item For a set $X$, $|X|$ denotes the cardinality of $X$. E.g.\
  $\bigl|[n]\bigr| = n$.
\item Vectors are one-indexed and generally column vectors.
\item For a natural number $i \in \Npos$, $e_i$ refers to the $i$th
  standard unit vector in some Euclidean space, which is usually
  defined by the context $e_i$ appears in, e.g.,\
  $e_2 \eqdef (0, 1, 0, \dotsc, 0)^\T$.
\item By default we are only considering Euclidean spaces, and so for
  vectors $x, y \in \R^d$ $\dotprod{x}{y}$ refers to the dot product
  of the vectors and is defined as
  $\dotprod{x}{y} \eqdef \sum_{i=1}^d x_i y_i$.
\item For a vector $x \in \R^d$,
  $\|x\|_2 \eqdef \sqrt{\dotprod{x}{x}}$ is referred to as the
  $\ell_2$ norm of $x$,
  $\|x\|_0 \eqdef \bigl|\{i \mid x_i \neq 0\}\bigr|$ is the number of
  non-zero entries in $x$, and $\|x\|_\infty \eqdef \max_i |x_i|$ is
  referred to as the $\ell_\infty$ norm of $x$. When referring to a
  space as Euclidean, we mean that it consists of real valued vectors
  and is equipped with the dot product and $\ell_2$ norm. The
  Euclidean or $\ell_2$ distance between two vectors $x, y \in \R^d$
  is the $\ell_2$ norm of their difference, i.e., $\|x - y\|_2$.
% \item $\tail_k(x)$ denotes $x$ with its $k$ largest entries zeroed out.
% \item $\median_{i \in \mathcal{I}} f(i)$ is the median of the set
%   $\{f(i) \mid i \in \mathcal{I}\}$.
\item For real numbers $a, b, c \in \R$ where $c \geq 0$,
  $a = b \pm c$ is shorthand for $b - c \leq a \leq b + c$ or
  equivalently $a \in [b - c, b + c]$.
\item For real numbers $a, b \in \R$, $a \ll b$ denotes that $a$ is
  much less than $b$, but what ``much less'' means exactly is not
  specified.
\item $e$ denotes Euler's number, which is approximately $2.718$,
  $\log$ denotes the logarithm with some unspecified but constant
  base, $\ln$ denotes the natural logarithm with base $e$, and $\lg$
  denotes the logarithm with base 2.
\item For most of this thesis we are working in a strong variation of
  the real RAM computational model. As such we can perform common
  arithmetic operations on a constant number of real numbers in
  constant time, and furthermore we assume that we have access to
  perfectly random hash functions that can be evaluated in constant
  time.
\item For functions $f, g \colon \Rpos \to \Rpos$,
  $f(a) = \bigO(g(a))$ is shorthand for
  $\exists c, a_0 \in \Rpos : \forall a > a_0 : f(a) \leq c g(a)$, and
  $f(a) = \Omega(g(a))$ is shorthand for $g(a) = \bigO(f(a))$, and
  $f(a) = \Theta(g(a))$ is shorthand for
  $f(a) = \bigO(g(a)) \wedge f(a) = \Omega(g(a))$. Furthermore
  $f(a) = o(g(a))$ is defined as
  $\forall c \in \Rpos : \exists a_0 \in \Rpos : \forall a > a_0 :
  f(a) < c g(n)$, and $f(a) = \omega(g(a))$ is shorthand for
  $g(n) = o(f(n))$.
\item The Rademacher distribution is the uniform distribution on
  $\{-1, 1\}$. The Bernoulli distribution is a distribution on
  $\{0, 1\}$ and unless otherwise specified it is the uniform
  distribution on $\{0, 1\}$. The Gaussian distribution is also known
  as the normal distribution and its probability density function can
  be defined as
  $\textrm{pdf}_{\mathsf{Gauss}}(x) \eqdef (\sigma \sqrt{2 \pi})^{-1}
  e^{-((x - \mu)/\sigma)^2/2}$ for a mean $\mu$ and standard deviation
  $\sigma$, which are $0$ and $1$ respectively unless otherwise
  specified. $\mathcal{N}(\mu, \sigma)$ denotes the Gaussian
  distribution with those parameters.
\item For a random variable $x$ and a set $X$, $x \in_R X$ means that
  $x$ is distributed uniformly among the elements of $X$. For example,
  if $r$ is a Rademacher random variable, then $r \in_R \{-1, 1\}$.
\item The meaning of $\binom{a}{b}$ is slightly overloaded depending
  on the type of $a$ in the sense that when $a \in \Npos$ it is the
  binomial coefficient, but it if $a$ is a set, then $\binom{a}{b}$
  refers to the family of subsets of $a$ with cardinality $b$. A
  common example is that $X$ is set and $\binom{X}{2}$ is then the set
  of all unordered pairs of distinct elements of $X$.
\end{itemize}

As for variable names, we will strive to follow the following
conventions, when discussing Johnson--Lindenstrauss and related
topics.
\begin{itemize}
\item $m, d \in \Npos$ are dimensions of vector spaces and $m < d$. In
  the context of dimensionality reduction, $d$ is referred to as the
  source dimension and $m$ as the target dimension.
\item $x, y$ are vectors in some Euclidean space.
\item $\eps \geq 0$ is a distortion of a vector's norm or the a
  distortion of the distance between two vectors.
\item $\delta \in [0, 1]$ is a failure probability.
\item $f$ is the default name for a function and $h$ is specifically a
  hash function.
\item $\sigma \in \{-1, 1\}$ is a random variable sampled from the
  Rademacher distribution, or a function applying random signs to a
  vector $x \in \R^d$, i.e. $\sigma(x)_i = \sigma_i x_i$, where the
  $\sigma_i$s are sampled from a Rademacher distribution.
\item $s \in \Nzero$ is the number of nonzero entries in a column of a matrix.
\item $q \in [0, 1]$ is the ratio of entries in a column of a matrix that are
  nonzero.
\item $\nu \in [0, 1]$ is the $\ell_\infty / \ell_2$ ratio of a vector.
\item $D \in \R^{d \times d}$ is a square diagonal matrix with random
  Rademacher values along the diagonal. I.e., $D_{ij} = 0$ for
  $i \neq j$, and $\Pr[D_{ii} = 1] = \Pr[D_{ii} = -1] = 1/2$ for
  $i \in [d]$.
\item $H \in \{-1, 1\}^{d \times d}$ is a Walsh--Hadamard matrix,
  where $d$ is a power of 2. One way to define the Walsh--Hadamard
  matrix is as $H_{ij} \eqdef (-1)^{\dotprod{i - 1}{j - 1}}$ for all
  $i, j \in [d]$, where $\dotprod{a}{b}$ denote the dot product of the
  ($\lg d$)-bit vectors corresponding to the binary representation of
  the numbers $a, b \in \{0, \dotsc, d - 1\}$. Alternatively, the
  Walsh--Hadamard matrix can be defined recursively after defining the
  $2 \times 2$ matrix as a base case. Let $H^{(n)}$ denote the
  $n \times n$ Walsh--Hadamard matrix, then $H^{(d)} \eqdef H^{(2)} \otimes H^{(d/2)} =
  \begin{pmatrix}
    H^{(d/2)} & H^{(d/2)} \\
    H^{(d/2)} & -H^{(d/2)}
  \end{pmatrix}$, for all $d > 2$ that are powers of 2.
\end{itemize}

When citing papers we generally refer to the most recent version of a
work. Oftentimes that is the journal version of some paper that was
also published at a conference. The \hyperref[cha:bib]{bibliography}
references the older versions in the entries of the newer versions.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../thesis"
%%% End:
