\chapter{Deferred Proofs}
\label{cha:deferred-proofs-from-intro}

\section{\texorpdfstring{$k$}{k}-Means Cost is Pairwise Distances}
\label{sec:app:k-means-cost}

Let us first repeat the lemma to remind ourselves what we need to
show.
\begin{replemma}{thm:kmeans:cost_pwd}
  Let $k, d \in \Npos$ and $X_i \subset \R^d$ for
  $i \in \{1, \dotsc, k\}$, then
  \begin{equation*}
    \sum_{i = 1}^{k} \sum_{x \in X_i} \Big\|x - \frac{1}{|X_i|}\sum_{y \in X_i} y \Big\|_2^2
    = \frac{1}{2} \sum_{i = 1}^{k} \frac{1}{|X_i|} \sum_{x, y \in X_i}\|x - y\|_2^2.
  \end{equation*}
\end{replemma}

In order to prove \cref{thm:kmeans:cost_pwd} we will need the
following lemma.
\begin{lemma}
  \label{thm:app:mult_mean_is_zero}
  Let $d \in \Npos$ and $X \subset \R^d$ and define
  $\mu \eqdef \frac{1}{|X|}\sum_{x \in X} x$ as the mean of $X$, then it holds that
  \begin{equation*}
    \sum_{x, y \in X} \dotprod{x - \mu}{y - \mu} = 0.
  \end{equation*}
\end{lemma}
\begin{proof}[Proof of \cref{thm:app:mult_mean_is_zero}]
  The lemma follows from the definition of $\mu$ and the linearity of
  the real inner product.
  \begin{align*}
    \sum_{x, y \in X} \dotprod{x - \mu}{y - \mu}
    &= \sum_{x, y \in X} \big( \dotprod{x}{y} - \dotprod{x}{\mu} - \dotprod{y}{\mu} + \dotprod{\mu}{\mu} \big) \\
    &= \sum_{x, y \in X} \dotprod{x}{y}
      - \sum_{x \in X} 2|X| \dotprod{x}{\mu}
      + |X|^2 \dotprod{\mu}{\mu} \\
    &= \sum_{x, y \in X} \dotprod{x}{y}
      - 2 \sum_{x \in X} \dotprod[\big]{x}{\sum_{y \in X} y}
      + \dotprod[\big]{\sum_{x \in X} x}{\sum_{y \in X} y} \\
    &= \sum_{x, y \in X} \dotprod{x}{y}
      - 2 \sum_{x, y \in X} \dotprod{x}{y}
      + \sum_{x, y \in X} \dotprod{x}{y} \\
    &= 0.
  \end{align*}
\end{proof}

\begin{proof}[Proof of \cref{thm:kmeans:cost_pwd}]
  We will first prove an identity for each partition, so let
  $X_i \subseteq X \subset \R^d$ be any partition of the dataset $X$
  and define $\mu_i \eqdef \frac{1}{|X_i|}\sum_{x \in X_i} x$ as the
  mean of $X_i$.
  \begin{align*}
    \frac{1}{2|X_i|} \sum_{x, y \in X_i} \|x - y \|_2^2
    &= \frac{1}{2|X_i|} \sum_{x, y \in X_i} \|(x - \mu_i) - (y - \mu_i) \|_2^2 \\
    &= \frac{1}{2|X_i|} \sum_{x, y \in X_i} \big(
      \|x - \mu_i\|_2^2 +
      \|y - \mu_i\|_2^2 -
      2 \dotprod{x - \mu_i}{y - \mu_i}
      \big) \\
    % &= |X_i|\frac{1}{2|X_i|} \sum_{x \in X_i} 2 \|x - \mu_i\|_2^2
    %   - \frac{1}{2|X_i|} \sum_{x, y \in X_i} 2 \dotprod{x - \mu_i}{y - \mu_i} \\
    &= \sum_{x \in X_i} \|x - \mu_i\|_2^2
      - \frac{1}{2|X_i|} \sum_{x, y \in X_i} 2 \dotprod{x - \mu_i}{y - \mu_i} \\
    &= \sum_{x \in X_i} \|x - \mu_i\|_2^2,
  \end{align*}
  where the last equality holds by
  \cref{thm:app:mult_mean_is_zero}. We now substitute each term in the
  sum in \cref{thm:kmeans:cost_pwd} using the just derived identity:
  \begin{equation*}
    \sum_{i = 1}^{k} \sum_{x \in X_i} \Big\|x - \frac{1}{|X_i|}\sum_{y \in X_i} y \Big\|_2^2
    = \frac{1}{2} \sum_{i = 1}^{k} \frac{1}{|X_i|} \sum_{x, y \in X_i}\|x - y\|_2^2.
  \end{equation*}
\end{proof}


\section{Super Sparse DKS}
\label{sec:app:dks-supersparse}

The tight bounds on the performance of feature hashing presented in
\cref{thm:fh:main} can be extended to tight performance bounds for the
DKS construction. Recall that the DKS construction, parameterised by a
so-called column sparsity $s \in \Npos$, works by first mapping a
vector $x \in \R^d$ to an $x' \in \R^{sd}$ by duplicating each entry
in $x$ $s$ times and then scaling with $1/\sqrt{s}$, before applying
feature hashing to $x'$, as $x'$ has a more palatable
$\ell_\infty / \ell_2$ ratio compared to $x$. The setting for the
extended result is that if we wish to use the DKS construction but we
only need to handle vectors with a small $\|x\|_\infty / \|x\|_2$
ratio, we can choose a column sparsity smaller than the usual
$\Theta(\eps^{-1} \log \frac{1}{\delta} \log\frac{m}{\delta})$ and
still get the Johnson--Lindenstrauss guarantees. This is formalised in
\cref{thm:dks:supersparse}. The two pillars of \cref{thm:fh:main} we
use in the proof of \cref{thm:dks:supersparse} is that the feature
hashing tradeoff is tight and that we can force the DKS construction
to create hard instances for feature hashing.

\begin{repcorollary}{thm:dks:supersparse}
  Let $\nu_{\mathsf{DKS}} \in [1/\sqrt{d}, 1]$ denote the largest
  $\ell_{\infty} / \ell_2$ ratio required, $\nu_{\mathsf{FH}}$ denote
  the $\ell_{\infty} / \ell_2$ constraint for feature hashing as
  defined in \cref{thm:fh:main}, and $s_{\mathsf{DKS}} \in [m]$ as the
  minimum column sparsity such that the DKS construction with that
  sparsity is a JLD for the subset of vectors $x \in \R^d$ that
  satisfy $\|x\|_\infty / \|x\|_2 \leq \nu_{\mathsf{DKS}}$. Then
  \begin{equation}
    \label{eq:dks:supersparse}
    s_{\mathsf{DKS}} = \Theta\Bigl( \frac{\nu_{\mathsf{DKS}}^2}{\nu_{\mathsf{FH}}^2} \Bigr).
  \end{equation}
\end{repcorollary}
The upper bound part of the $\Theta$ in \cref{thm:dks:supersparse}
shows how sparse we can choose the DKS construction to be and still
get Johnson--Lindenstrauss guarantees for the data we care about,
while the lower bound shows that if we choose a sparsity below this
bound, there exists vectors who get distorted too much too often
despite having an $\ell_\infty / \ell_2$ ratio of at most
$\nu_{\mathsf{DKS}}$.

\begin{proof}[Proof of~\cref{thm:dks:supersparse}]
  Let us first prove the upper bound:
  $s_{\mathsf{DKS}} = \bigO\bigl(
  \frac{\nu_{\mathsf{DKS}}^2}{\nu_{\mathsf{FH}}^2} \bigr)$.

  Let
  $s \eqdef \Theta\bigl( \frac{\nu_{\mathsf{DKS}}^2}{\nu_{\mathsf{FH}}^2}
  \bigr) \in [m]$ be the column sparsity, and let $x \in \R^d$ be a
  unit vector with $\|x\|_\infty \leq \nu_{\mathsf{DKS}}$. The goal is
  now to show that a DKS construction with sparsity $s$ can embed $x$
  while preserving its norm within $1 \pm \eps$ with probability at
  least $1 - \delta$ (as defined in \cref{thm:distjl}). Let
  $x' \in \R^{sd}$ be the unit vector constructed by duplicating each
  entry in $x$ $s$ times and scaling with $1/\sqrt{s}$ as in the DKS
  construction. We now have
  \begin{equation}
    \label{eq:dks_cor:xprime_infty_upper}
    \|x'\|_\infty \leq \frac{\nu_{\mathsf{DKS}}}{\sqrt{s}} = \Theta(\nu_{\mathsf{FH}}).
  \end{equation}

  Let $\mathsf{DKS}$ denote the JLD from the DKS construction with
  column sparsity $s$, and let $\mathsf{FH}$ denote the feature
  hashing JLD. Then we can conclude
  \begin{align*}
    \Pr_{f \sim \mathsf{DKS}}\Bigl[ \bigl| \|f(x)\|_2^2 - 1 \bigr| \leq \eps \Bigr]
    &= \Pr_{g \sim \mathsf{FH}}\Bigl[ \bigl| \|g(x')\|_2^2 - 1 \bigr| \leq \eps \Bigr]
      \geq 1 - \delta,
  \end{align*}
  where the inequality is implied by \cref{eq:dks_cor:xprime_infty_upper,thm:fh:main}.

  Now let us prove the lower bound:
  $s_{\mathsf{DKS}} = \Omega\bigl(
  \frac{\nu_{\mathsf{DKS}}^2}{\nu_{\mathsf{FH}}^2} \bigr)$.

  Let
  $s \eqdef o\bigl( \frac{\nu_{\mathsf{DKS}}^2}{\nu_{\mathsf{FH}}^2}
  \bigr)$, and let
  $x = (\nu_{\mathsf{DKS}}, \dotsc, \nu_{\mathsf{DKS}}, 0, \dotsc,
  0)^\T \in \R^d$ be a unit vector. We now wish to show that a DKS
  construction with sparsity $s$ will preserve the norm of $x$ to
  within $1 \pm \eps$ with probability strictly less than
  $1 - \delta$. As before, define $x' \in \R^{sd}$ as the unit vector
  the DKS construction computes when duplicating every entry in $x$
  $s$ times and scaling with $1/\sqrt{s}$. This gives
  \begin{equation}
    \label{eq:dks_cor:xprime_infty_lower}
    \|x'\|_\infty = \frac{\nu_{\mathsf{DKS}}}{\sqrt{s}} = \omega(\nu_{\mathsf{FH}}).
  \end{equation}

  Finally, let $\mathsf{DKS}$ denote the JLD from the DKS construction with
  column sparsity $s$, and let $\mathsf{FH}$ denote the feature
  hashing JLD. Then we can conclude
  \begin{align*}
    \Pr_{f \sim \mathsf{DKS}}\Bigl[ \bigl| \|f(x)\|_2^2 - 1 \bigr| \leq \eps \Bigr]
    &= \Pr_{g \sim \mathsf{FH}}\Bigl[ \bigl| \|g(x')\|_2^2 - 1 \bigr| \leq \eps \Bigr]
      < 1 - \delta,
  \end{align*}
  where the inequality is implied by
  \cref{eq:dks_cor:xprime_infty_lower,thm:fh:main}, and the fact
  that $x'$ has the shape of an asymptotically worst case instance for
  feature hashing.
\end{proof}


\section{LWTJL Fails for Too Sparse Vectors}
\label{sec:app:lwt-nu-bound}

\begin{repproposition}{thm:lwt:nu_upper_bound}
  For any seed matrix define $\mathsf{LWT}$ as the LWTJL distribution
  seeded with that matrix. Then for all $\delta \in (0, 1)$, there
  exists a vector $x \in \CC^d$ (or $x \in \R^d$, if the seed matrix
  is a real matrix) satisfying
  $\|x\|_\infty / \|x\|_2 = \Theta(\log^{-1/2} \frac{1}{\delta})$ such
  that
  \begin{equation*}
    \Pr_{f \sim \mathsf{LWT}}[f(x) = \zerovector] > \delta.
  \end{equation*}
\end{repproposition}
\begin{proof}
  The main idea is to construct the vector $x$ out of segments that
  are orthogonal to the seed matrix with some probability, and then
  show that $x$ is orthogonal to all copies of the seed matrix
  simultaneously with probability larger than $\delta$.

  Let $r, c \in \Npos$ be constants and $A_1 \in \CC^{r \times c}$ be a
  seed matrix. Let $d$ be the source dimension of the LWTJL
  construction, $D \in \{-1, 0, 1\}^{d \times d}$ be the random
  diagonal matrix with i.i.d. Rademachers, $l \in \Npos$ such that
  $c^l = d$, and $A_l \in \CC^{r^l \times c^l}$ be the the LWT, i.e.
  $A_l \eqdef A_1^{\otimes l}$. Since $r < c$ there exists a
  nontrivial vector $z \in \CC^c \setminus \{\zerovector\}$ that is orthogonal
  to all $r$ rows of $A_1$ and $\|z\|_\infty = \Theta(1)$. Now define
  $x \in \CC^d$ as $k \in \Npos$ copies of $z$ followed by a padding of
  $0$s, where
  $k = \lfloor \frac{1}{c} \lg \frac{1}{\delta} - 1\rfloor$. Note that
  if the seed matrix is real, we can choose $z$ and therefore $x$ to
  be real as well.

  The first thing to note is that
  \begin{equation*}
    \|x\|_0 \leq ck < \lg \frac{1}{\delta},
  \end{equation*}
  which implies that
  \begin{equation*}
    \Pr_D[Dx = x] = 2^{-\|x\|_0} > \delta.
  \end{equation*}

  Secondly, due to the Kronecker structure of $A_l$ and the fact that
  $z$ is orthogonal to the rows of $A_1$, we have
  \begin{equation*}
    Ax = \zerovector.
  \end{equation*}
  Taken together, we can conclude
  \begin{equation*}
    \Pr_{f \sim \mathsf{LWT}}[f(x) = \zerovector] \geq \Pr_D[A_lDx = \zerovector] \geq \Pr_D[Dx = x] > \delta.
  \end{equation*}


  Now we just need to show that
  $\|x\|_\infty / \|x\|_2 = \Theta(\log^{-1/2} \frac{1}{\delta})$.
  Since $c$ is a constant and $x$ is consists of
  $k = \Theta(\log \frac{1}{\delta})$ copies of $z$ followed by
  zeroes,
  \begin{alignat*}{3}
    \|x\|_\infty &= \|z\|_\infty &&= \Theta(1), \\
    \|z\|_2 &= \Theta(1), \\
    \|x\|_2 &= \sqrt{k} \|z\|_2 &&= \Theta\bigl(\sqrt{\log \frac{1}{\delta}}\bigr),
  \end{alignat*}
  which implies the claimed ratio,
  \begin{equation*}
    \frac{\|x\|_\infty}{\|x\|_2} = \Theta(\log^{-1/2} \frac{1}{\delta}).
  \end{equation*}
\end{proof}

The following corollary is just a restatement of
\cref{thm:lwt:nu_upper_bound} in terms of \cref{thm:distjl}, and the
proof therefore follows immediately from
\cref{thm:lwt:nu_upper_bound}.
\begin{corollary}
  \label{thm:lwt:nu_upper_bound:corollary}
  For every $m, d, \in \Npos$, and $\delta, \eps \in (0, 1)$, and LWTJL
  distribution $\mathsf{LWT}$ over $f \colon \K^d \to \K^m$, where
  $\K \in \{\R, \CC\}$ and $m < d$ there exists a vector $x \in \K^d$
  with $\|x\|_\infty / \|x\|_2 = \Theta(\log^{-1/2} \frac{1}{\delta})$
  such that
  \begin{equation*}
    \Pr_{f \sim \mathsf{LWT}}\Bigl[ \, \bigl| \,\|f(x)\|_2^2 - \|x\|_2^2\, \bigr| \leq \eps \|x\|_2^2 \,\Bigr] < 1 - \delta .
  \end{equation*}
\end{corollary}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "ms"
%%% End:
