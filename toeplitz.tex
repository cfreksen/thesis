\chapter[Toeplitz]{Toeplitz \\ \textit{\normalsize This chapter is based on~\citep{Freksen:2020:oUTaCMfJLT}}}
\label{cha:toeplitz}

As mentioned in \cref{cha:results}, the main result in
\citep{Freksen:2020:oUTaCMfJLT} unfortunately shows that the analysis
of Vybíral~\cite{Vybiral:2011:aVotJLLfCM} cannot be tightened to give
an even lower target dimensionality when preserving norms. More
specifically, we prove that the upper bound given in
\cite{Vybiral:2011:aVotJLLfCM} is optimal.
\begin{theorem}
  \label{thm:toeplitz:highlevelmain}
  Let $d, m \in \Npos$ be the source and target dimension with $m < d$
  and let $T \in \{-1, 1\}^{m \times d}$ and
  $D \in \{-1, 0, 1\}^{d \times d}$ be the Toeplitz and diagonal
  matrix in the JLD proposed in \citep{Hinrichs:2011:JLLfCM}. For all
  distortions $0 < \eps < C$, where $C$ is a universal constant, and
  any desired error probability $\delta > 0$, if the following holds
  for every unit vector $x \in \R^d$:
  \begin{equation*}
    \Pr\bigg[\Big|\big\|\frac{1}{\sqrt{m}}TDx\big\|_2^2 - 1\Big| \leq
    \eps\bigg] \geq 1 - \delta,
  \end{equation*}
  then it must be the case that
  $m = \Omega(\eps^{-2} \log^2 \frac{1}{\delta})$.
\end{theorem}

While \cref{thm:toeplitz:highlevelmain} already shows that one cannot
tighten the analysis of Vybíral for preserving the norm of just one
vector, \cref{thm:toeplitz:highlevelmain} does leave open the
possibility that one would not need to union bound over all
$\binom{N}{2}$ pairs of difference vectors when trying to preserve
all pairwise distances amongst a set of $N$ vectors.

\citet{Krahmer:2011:NaIJLEvtRIP} avoided this union bound by
introducing a dependency on the original dimension $d$: Using the
restricted isometry property they show that to preserve pairwise
distances in a set $X \subset \R^d$ of $n$ vectors to within
$(1 \pm \eps)$ the target dimension
$m = \Theta\bigl( \eps^{-1} \log^{3/2} |X| \log^{3/2} d + \eps^{-2}
\log |X| \log^4 d \bigr)$ suffices for the Toeplitz construction,
which for some range of parameters is an improvement on
\citep{Vybiral:2011:aVotJLLfCM}. However, we show that there is some
range of parameters where the upper bound in
\citep{Vybiral:2011:aVotJLLfCM} is tight with respect to pairwise
distances.
\begin{theorem}
  \label{thm:toeplitz:highlevelmainMany}
  Let $d, m \in \Npos$ be the source and target dimension with
  $m < d$, let $n \in \Npos$ be the set size, and let
  $T \in \{-1, 1\}^{m \times d}$ and $D \in \{-1, 0, 1\}^{d \times d}$
  be the Toeplitz and diagonal matrix in the JLD proposed in
  \citep{Hinrichs:2011:JLLfCM}. For all distortions $0 < \eps < C$,
  where $C$ is a universal constant, if the following holds for every
  set of $n$ vectors $X \subset \R^d$:
  \begin{equation*}
    \Pr\bigg[\forall x,y \in X : \Big|\big\|\frac{1}{\sqrt{m}}TDx -
          \frac{1}{\sqrt{m}}TDy\big\|_2^2  - \| x-y\|_2^2\Big|  \leq
          \eps\|x-y\|_2^2 \bigg] = \Omega(1),
  \end{equation*}
  then it must be the case that either $m = \Omega(\eps^{-2} \log^2 n)$
  or $m = \Omega(d/n)$.
\end{theorem}

We remark that our proofs also work if we replace $T$ be a partial
circulant matrix (which was also proposed in
\citep{Hinrichs:2011:JLLfCM, Vybiral:2011:aVotJLLfCM}). Furthermore,
we expect that minor technical manipulations to our proof would also
show the above theorems when the entries of $T$ and $D$ are
$\mathcal{N}(0,1)$ distributed rather than Rademacher (this was also
proposed in \citep{Hinrichs:2011:JLLfCM, Vybiral:2011:aVotJLLfCM}).

\section{Lower Bound for One Vector}
\label{sec:toeplitz:onevector}
Let $T$ be a $m \times d$ Toeplitz matrix defined from random
variables $t_{-(m-1)}, t_{-(m-2)}, \dots, t_{d-1}$ such that entry
$T_{ij}$ takes values $t_{j-i}$ for $i \in [m]$ and $j \in [d]$. Let
$D$ be a $d \times d$ diagonal matrix with entries $D_{ii} = \sigma_i$
for $i \in [d]$. The $t_i$s and the $\sigma_i$s are i.i.d. Rademacher
random variables. This section shows the following.
\begin{theorem}\label{thm:toeplitz:main}
  Let $T$ be $m \times d$ Toeplitz and $D$ $d \times d$ diagonal. If
  $t_{-(m-1)},t_{-(m-2)}, \dotsc, t_{d-1}$ and
  $\sigma_1, \dotsc, \sigma_d$ are independently distributed
  Rademacher random variables, then for all $0 < \eps < C$, where $C$
  is a universal constant, there exists a unit vector $x \in \R^d$
  such that
  \begin{equation*}
    \Pr\bigg[\Big|\big\|\frac{1}{\sqrt{m}}TDx\big\|_2^2 - 1\Big| > \eps\bigg] \geq 2^{-\bigO(\eps \sqrt{m})}.
  \end{equation*}
  and furthermore, all but the first $O(\sqrt{m})$ coordinates of $x$
  are $0$.
\end{theorem}
It follows from \cref{thm:toeplitz:main} that if we want to have
probability at least $1-\delta$ of preserving the norm of \emph{any}
unit vector $x$ to within $(1 \pm \eps)$, it must be the case that
$\eps \sqrt{m} = \Omega(\log \frac{1}{\delta})$, i.e.\@
$m = \Omega(\eps^{-2} \log^2 \frac{1}{\delta})$. This is precisely the
statement of \cref{thm:toeplitz:highlevelmain}. Thus we set out to
prove \cref{thm:toeplitz:main}.

To prove \cref{thm:toeplitz:main}, we wish to invoke the Paley--Zygmund
inequality, which states, that if $X$ is a non-negative random
variable with finite variance and $0 \leq \theta \leq 1$, then
\begin{equation*}
  \Pr [X > \theta \E[X]] \geq (1 - \theta)^2 \frac{\E^2[X]}{\E[X^2]}.
\end{equation*}

We carefully choose a unit vector $x$, and define the random variable
for Paley--Zygmund to be the $r$'th moment of the difference between
the norm of $x$ transformed and 1.
\begin{proof}
  Let $r$ be an even positive integer less than $m/4$ and define
  $k \eqdef 4r$. Note that $k \leq m$. Let $x$ be an arbitrary
  $d$-dimensional unit vector such that the first $k$ coordinates are
  in $\{-1/\sqrt{k}, +1/\sqrt{k}\}$, while the remaining $d - k$
  coordinates are 0. Define the random variable parameterised by $r$
  \begin{equation*}
    Z_r := \Big(\big\|\frac{1}{\sqrt{m}} TDx\big\|_2^2 - 1 \Big)^r.
  \end{equation*}
  Since $r$ is even, the random variable $Z_r$ is non-negative.

  We wish to lower-bound $\E[Z_r]$ and upper-bound $\E[Z_r^2]$ in
  order to invoke Paley--Zygmund. The bounds we prove are as follows.
  \begin{lemma}
    \label{lem:toeplitz:boundZ}
    If $r \leq \sqrt{m}$, then the random variable $Z_r$ satisfies
    \begin{flalign*}
      && \E[Z_r] &\geq m^{-r/2} r^r 2^{-\bigO(r)} & \\
      \text{and} && \E[Z_r^2] &\leq m^{-r} r^{2r} 2^{\bigO(r)}. &
    \end{flalign*}
  \end{lemma}

  Before proving \cref{lem:toeplitz:boundZ} we show how to use it together
  with Paley--Zygmund to complete the proof of \cref{thm:toeplitz:main}.

  We start by invoking Paley--Zygmund and then rewriting the
  expectations according to \cref{lem:toeplitz:boundZ},
  \begin{align*}
    \Pr[Z_r > \E[Z_r]/2] &\geq (1/4)\frac{\E^2[Z_r]}{\E[Z_r^2]} \implies \\
    \Pr\bigl[Z_r^{1/r} > (\E[Z_r]/2)^{1/r}\bigr] &\geq (1/4)\frac{\E^2[Z_r]}{\E[Z_r^2]} \implies \\
    \Pr\biggl[\Bigl|\bigl\|\frac{1}{\sqrt{m}} TDx\bigr\|_2^2 - 1 \Bigr| >
    \frac{r}{C_0 \sqrt{m}}\biggr] &\geq 2^{-\bigO(r)}.
  \end{align*}
  Here $C_0$ is some constant greater than $0$.  For any
  $0 < \eps < 1/C_0$, we can now set $r$ such that
  $r/(C_0 \sqrt{m}) = \eps$, i.e.\@ we choose $r = \eps C_0
  \sqrt{m}$. This choice of $r$ satisfies $r \leq \sqrt{m}$ as
  required by \cref{lem:toeplitz:boundZ}. We have thus shown that
  \begin{equation*}
    \Pr\biggl[\Bigl|\bigl\|\frac{1}{\sqrt{m}} TDx\bigr\|_2^2 - 1 \Bigr| >
    \eps \biggr] \geq 2^{-\bigO(\eps \sqrt{m})}.
  \end{equation*}
\end{proof}

\begin{remark}
  \Cref{thm:toeplitz:main} can easily be extended to partial circulant
  matrices.  The difference between partial circulant and Toeplitz
  matrices is the dependence between the values in the first $m$ and
  last $m$ columns. However, as only the first
  $k = 4r \leq 4 \sqrt{m}$ entries in $x$ are nonzero, the last $m$
  columns are ignored, and so partial circulant and Toeplitz matrices
  behave identically in our proof.
\end{remark}

\begin{proof}[Proof of \cref{lem:toeplitz:boundZ}]
  Before we prove the two bounds in \cref{lem:toeplitz:boundZ}
  individually, we rewrite $\E[Z_r]$, as this benefits both proofs.
  \begin{align*}
    \E[Z_r] &= \E\Bigg[\bigg(\big\|\frac{1}{\sqrt{m}} TDx\big\|_2^2 - 1 \bigg)^r\Bigg] \\
            &= \E\Bigg[\bigg(\Big(\frac{1}{m}\sum_{i=1}^m \big(\sum_{j=1}^n t_{j-i} \sigma_j x_j\big)^2 \Big)- 1 \bigg)^r\Bigg] \\
            &= \E\Bigg[\bigg(\Big(\frac{1}{m}\sum_{i=1}^m \big((\sum_{j=1}^n t^2_{j-i} d^2_j x^2_j) +
              (\sum_{j=1}^n \sum_{h \in \{1,\dots,n\} \setminus \{j\}} t_{j-i}t_{h-i} \sigma_j \sigma_h x_j x_h ) \big)\Big)- 1 \bigg)^r\Bigg] \\
            &= \E\Bigg[\bigg(\frac{1}{m}\sum_{i=1}^m \big((\sum_{j=1}^n t^2_{j-i} d^2_j x^2_j - x_j^2) +
              (\sum_{j=1}^n \sum_{h \in \{1,\dots,n\} \setminus \{j\}} t_{j-i}t_{h-i} \sigma_j \sigma_h x_j x_h  ) \big)\bigg)^r\Bigg] \\
            &= \E\Bigg[\bigg(\frac{1}{m}\sum_{i=1}^m \sum_{j=1}^n \sum_{h \in \{1,\dots,n\} \setminus \{j\}} t_{j-i}t_{h-i} \sigma_j \sigma_h x_j x_h \bigg)^r\Bigg] \\
            &= \frac{1}{m^{r}} \sum_{\substack{S \in ([m] \times [n] \times [n])^r \\ \forall (i,j,h) \in S : h \neq j}} \E\Big[ \prod_{(i,j,h) \in S} t_{j-i}t_{h-i}\sigma_j \sigma_h x_j x_h \Big].
  \end{align*}
  Observe that for $j > k$ or $h > k$ the product becomes $0$, as
  either $x_j$ or $x_h$ is 0. By removing all these terms, we simplify
  the sum to
  \begin{equation*}
    \E[Z_r] = \frac{1}{m^{r}} \sum_{\substack{S \in ([m] \times [k] \times [k])^r \\   \forall (i,j,h) \in S : h \neq j}} \E\Big[ \prod_{(i,j,h) \in S} t_{j-i}t_{h-i}\sigma_j \sigma_h x_j x_h \Big].
  \end{equation*}

  Observe for $S \in ([m] \times [k] \times [k])^r$, that the value
  $\E\Big[ \prod_{(i,j,h) \in S} t_{j-i}t_{h-i} \sigma_j \sigma_h x_j x_h \Big]$
  is $0$ if at least one of the following two things are true
  \begin{itemize}
  \item A $\sigma_j$ occurs an odd number of times in the product.
  \item A variable $t_a$ occurs an odd number of times in the product.
  \end{itemize}
  To see this, note that by the independence of the random variables,
  we can write the expectation of the product, as a product of
  expectations where each term in the product has all the occurrences
  of the same random variable. Since the $\sigma_j$'s and $t_a$'s are
  Rademachers, the expectation of any odd power of one of these random
  variables is $0$. Thus if just a single random variable amongst the
  $\sigma_j$'s and $t_a$'s occurs an odd number of times, we have
  $\E\Big[ \prod_{(i,j,h) \in S} t_{j-i}t_{h-i}\sigma_j \sigma_h x_j x_h \Big] =
  0$. Similarly, we observe that if every random variable occurs an
  even number of times, then the expectation of the product is exactly
  $1/k^r$ since each $x_j$ also occurs an even number of times. If
  $\Gamma_r$ denotes the number of tuples
  $S \in ([m] \times [k] \times [k])^r$ such that
  $\forall (i,j,h) \in S$ we have $h \neq j$ and furthermore:
  \begin{itemize}
  \item For all columns $a \in [k]$, $\bigl|\{(i, j, h) \in S \mid  j=a \vee h=a\}\bigr| \mod 2 = 0$;
  \item For all diagonals $a \in \{-(m-1), \dotsc, k-1\}$, $\bigl|\{(i,j,h) \in S \mid  j-i=a \vee h-i=a\}\bigr| \mod 2 = 0$.
  \end{itemize}
  Then we conclude
  \begin{equation}
    \label{eq:toeplitz:e_zk_rewrite}
    \E[Z_r] = \frac{\Gamma_r}{k^r m^r}.
  \end{equation}

  Note that $Z_r^2 = Z_{2r}$. Therefore,
  \begin{equation}
    \label{eq:toeplitz:e_z2k_rewrite}
    \E[Z_r^2] = \E[Z_{2r}] = \frac{\Gamma_{2r}}{k^{2r} m^{2r}}.
  \end{equation}

  To complete the proof of \cref{lem:toeplitz:boundZ} we need lower and
  upper bounds for $\Gamma_r$ and $\Gamma_{2r}$. The bounds we prove
  are the following.
  \begin{lemma}
    \label{lem:toeplitz:boundGamma}
    If $r \leq \sqrt{m}$, then $\Gamma_r$ and $\Gamma_{2r}$ satisfy
    \begin{flalign*}
      && \Gamma_r &= m^{r/2} k^r r^r 2^{-\bigO(r)} & \\
      \text{and} && \Gamma_{2r} &= m^{r} k^{2r} r^{2r} 2^{\bigO(r)}. &
    \end{flalign*}
  \end{lemma}

  The proofs of the two bounds in \cref{lem:toeplitz:boundGamma} are given
  in \cref{sec:toeplitz:lower-bound-t,sec:toeplitz:upper-bound-t}.

  Substituting the bounds from \cref{lem:toeplitz:boundGamma} in
  \cref{eq:toeplitz:e_zk_rewrite} and \cref{eq:toeplitz:e_z2k_rewrite} we get
  \begin{align*}
    \E[Z_r] &= m^{-r/2} r^r 2^{-\bigO(r)} \\
    \E[Z_r^2] &= m^{-r} r^{2r} 2^{\bigO(r)},
  \end{align*}
  which are the bounds we sought for \cref{lem:toeplitz:boundZ}.
\end{proof}

\subsection{Lower-Bounding \texorpdfstring{$\Gamma_r$}{\Gamma{}r}}
\label{sec:toeplitz:lower-bound-t}

We first recall that the definition of $\Gamma_r$ is the number of
tuples $S \in ([m] \times [k] \times [k])^r$ satisfying that
$\forall (i, j, h) \in S$ we have $h \neq j$ and furthermore:
\begin{itemize}
\item For all columns $a \in [k]$,
  $\bigl|\{(i, j, h) \in S \mid j=a \vee h=a\} \bigr| \mod 2 = 0$;
\item For all diagonals $a \in \{-(m-1), \dotsc, k-1\}$,
  $\bigl|\{(i, j, h) \in S \mid j-i=a \vee h-i=a\} \bigr| \mod 2 = 0$.
\end{itemize}

We view a triple $(i, j, h) \in ([m] \times [k] \times [k])$ as two
entries $(i, j)$ and $(i, h)$ in an $m \times k$ matrix. Furthermore,
when we say that a triple \emph{touches} a column or diagonal, a
matrix entry of the triple lies on that column or diagonal, so
$(i, j, h)$ touches columns $j$ and $h$ and diagonals $j - i$ and
$h - i$. Similarly, we say that a tuple
$S \in ([m] \times [k] \times [k])^r$ touches a given column or
diagonal $l$ times, if $l$ triples in $S$ touch that column or
diagonal.

We intend to prove a lower bound for $\Gamma_r$ by constructing a big
family $\mathcal{F} \subseteq ([m] \times [k] \times [k])^r$ of
tuples, where each tuple satisfies that each column and diagonal
touched by that tuple is touched exactly twice. As each column and
diagonal is touched an even number of times, the number of tuples in
the family is a lower bound for $\Gamma_r$.
\begin{proof}[Proof of $\Gamma_r = m^{r/2} k^r r^r 2^{-\bigO(r)}$]
  We describe how to construct a family
  $\mathcal{F} \subseteq ([m] \times [k] \times [k])^r$ of tuples
  satisfying that $\forall S \in \mathcal{F}, \forall (i, j, h) \in S$
  we have $h \neq j$ and furthermore:
  \begin{itemize}
  \item For all columns $a \in [k]$,
    $\bigl|\{(i, j, h) \in S \mid j=a \vee h=a\}\bigr| \in \{0, 2\}$;
  \item For all diagonals $a \in \{-(m-1), \dotsc ,k-1\}$,
    $\bigl|\{(i, j, h) \in S \mid j-i=a \vee h-i=a\}\bigr| \in \{0, 2\}$.
  \end{itemize}

  From this and the definition of $\Gamma_r$ it is clear that
  $|\mathcal{F}| \leq \Gamma_r$.

  When constructing $S \in \mathcal{F}$, we view $S$ as consisting of
  two halves $S_1$ and $S_2$, such that $S_1$ touches exactly the same
  columns and diagonals as $S_2$ and both $S_1$ and $S_2$ touch each
  column and diagonal at most once. To capture this, we give the
  following definition, where $\mathbb{S}$ is meant to be the family
  of such halves $S_1$ and $S_2$.
  \begin{definition}
    \label{def:toeplitz:bbS}
    Let $\mathbb{S}$ be the set of all tuples
    $S \in ([m] \times [k] \times [k])^{r/2}$ such that
    \begin{itemize}
    \item $\forall (i, j, h) \in S, h \neq j$;
    \item For all columns
      $a \in [k], \bigl|\{(i, j, h) \in S \mid j = a \vee h = a\}\bigr| \leq 1$;
    \item For all diagonals
      $a \in \{-(m-1), \dotsc, k - 1\}, \bigl|\{(i, j, h) \in S \mid j - i
      = a \vee h - i = a\}\bigr| \leq 1$.
    \end{itemize}
  \end{definition}

  \Cref{def:toeplitz:bbS} mimics the definition of $\Gamma_r$, and
  the first item in \cref{def:toeplitz:bbS} ensures that the triples
  in a tuple in $\mathbb{S}$ are of the same form as in
  $\Gamma_r$. The final two items ensure that each column and
  diagonal, respectively, is touched at most once. This is exactly the
  properties we wanted of $S_1$ and $S_2$ individually.

  We can now construct $\mathcal{F}$ as all pairs of (half) tuples
  $S_1, S_2 \in \mathbb{S}$, such that $S_1$ touches exactly the same
  columns and diagonals as $S_2$. To capture that $S_1$ and $S_2$
  touch the same columns and diagonals, we introduce the notion of a
  signature. A signature of $S_i$ is the set of columns and diagonals
  touched by $S_i$.

  To have $S_1$ and $S_2$ touch exactly the same columns and
  diagonals, it is necessary and sufficient that they have the same
  signature.

  We introduce the following notation: $B$ denotes the number of
  signatures with at least one member, and by enumerating the
  signatures, we let $b_i$ denote the number of (half) tuples in
  $\mathbb{S}$ with signature $i$.

  We recall that a (half) tuple $S_1 \in \mathbb{S}$ touches each
  column and diagonal at most once, and if $S_1$ and $S_2$ share the
  same signature, they touch exactly the same columns and
  diagonals. Therefore, using $\circ$ to mean concatenation,
  $S = S_1 \circ S_2 \in \mathcal{F}$, as each column and diagonal
  touched is touched exactly twice. Therefore $|\mathcal{F}|$ is a
  lower bound for $\Gamma_r$. Note that for a given signature $i$, the
  number of choices of $S_1$ and $S_2$ with that signature is
  $b_i^2$. This gives the following inequality,
  \begin{equation*}
    \Gamma_r \geq |\mathcal{F}| = \sum_{i=1}^B b_i^2.
  \end{equation*}

  We now apply the Cauchy--Schwarz inequality:
  \begin{equation}
    \sum_{i=1}^B b_i^2 \sum_{i=1}^B 1^2 \geq \bigl(\sum_{i=1}^B b_i \bigr)^2 \implies
    \sum_{i=1}^B b_i^2 \geq \frac{\bigl(\sum_{i=1}^B b_i \bigr)^2}{\sum_{i=1}^B 1^2} \implies
    \label{eq:toeplitz:lower_bound_sum_bi2}
    \Gamma_r \geq \frac{|\mathbb{S}|^2}{B}.
  \end{equation}

  To get a lower bound on $|\mathbb{S}|^2/B$ (and in turn $\Gamma_r$),
  we need a lower bound on $|\mathbb{S}|$ and an upper bound on
  $B$. These bounds are stated in the following lemmas.
  \begin{lemma}
    \label{lem:toeplitz:select_distinct_mnn}
    $|\mathbb{S}| = \Omega(m^{r/2} k^r 2^{-r})$.
  \end{lemma}
  \begin{lemma}
    \label{lem:toeplitz:num_sigs}
    $ B = \bigO\Bigl(\binom{m + k}{r/2} s^{r/2} \binom{k}{r} \Bigr) $.
  \end{lemma}

  Before proving any of these lemmas, we show that they together with
  \cref{eq:toeplitz:lower_bound_sum_bi2} give the desired lower bound on
  $\Gamma_r$:
  \begin{align}
    \Gamma_r &= \frac
          {\Omega(m^{r/2} k^r 2^{-r})^2}
          % --------------------
          {\bigO\Big(\binom{m + k}{r/2} k^{r/2} \binom{k}{r} \Big)}
    \label{eq:toeplitz:gamma_lower_ugly}
             = \Omega\Big(\frac
          {m^r k^{2r} 2^{-2r} (r/2)^{r/2} r^r}
          % --------------------
          {(m + k)^{r/2} k^{r/2} k^r}
          \Big).
  \end{align}

  Because $k = 4r$, we have
  $\frac{(r/2)^{r/2}}{k^{r/2}} = 2^{-\Theta(r)}$, and because
  $k \leq m$, $\frac{m^r}{(m+k)^{(r/2)}} = m^{r/2}
  2^{-\Theta(r)}$. With this we can simplify
  \cref{eq:toeplitz:gamma_lower_ugly} as
  \begin{align*}
    \Gamma_r &= m^{r/2} k^r r^r 2^{-\bigO(r)},
  \end{align*}
  which is the lower bound we sought.
\end{proof}

\begin{proof}[Proof of \cref{lem:toeplitz:select_distinct_mnn}]
  Recall that $\mathbb{S} \subseteq ([m] \times [k] \times [k])^{r/2}$
  is the set of (half) tuples that touch each column and diagonal at
  most once, and, for each triple $(i, j, h)$ in these (half) tuples,
  we have $j \neq h$.

  We prove \cref{lem:toeplitz:select_distinct_mnn} by analysing how we can
  create a large number of distinct $S \in \mathbb{S}$ by choosing the
  triples in $S$ iteratively.

  For each triple, we choose a row and two distinct entries on this
  row. We choose the row among any of the $m$ rows.

  However, because $S \in \mathbb{S}$, when choosing entries on the
  row, we cannot choose entries that lie on columns or diagonals
  touched by previously chosen triples. Instead we choose the two
  entries among any of the other entries. Therefore, whenever we
  choose a triple, this triple prevents at most four row entries from
  being chosen for every subsequent triple, as the two diagonals and
  two columns touched by the chosen triple intersect with at most four
  entries on each of the rows of the subsequent triples. This leads to
  the following recurrence, describing a lower bound for the number of
  triples
  \begin{align}
    \label{eq:toeplitz:select_distinct_mnn:recurr}
    F(\rho, \kappa, \tau) =
    \begin{cases}
      \rho \cdot \kappa \cdot (\kappa - 1) \cdot F(\rho, \kappa - 4, \tau - 1) & \text{if } \tau > 0 \\
      1 & \text{otherwise}
    \end{cases}
  \end{align}
  where $\rho$ is the number of rows to choose from, $\kappa$ is the minimum
  number of choosable entries in any row, and $\tau$ is the number of
  triples left to choose.

  Inspecting \cref{eq:toeplitz:select_distinct_mnn:recurr}, we can see that
  $F$ can equivalently be defined as
  \begin{align}
    \label{eq:toeplitz:select_distinct_mnn:recurr_simpl}
    F(\rho, \kappa, \tau) &= \rho^\tau \prod_{i = 0}^{\tau-1} (\kappa - 4i) (\kappa - 1 - 4i).
  \end{align}

  If $\tau \leq \frac{\kappa}{8}$ then the terms inside the product in
  \cref{eq:toeplitz:select_distinct_mnn:recurr_simpl} are greater than
  $\frac{\kappa}{2}$, so we can bound $F$ from below:
  \begin{align*}
    F(\rho, \kappa, \tau) &\geq \rho^\tau \bigl( \frac{\kappa}{2} \bigr)^{2\tau} = \rho^\tau \kappa^{2\tau} \frac{1}{4^\tau}.
  \end{align*}

  We now insert the values for $\rho$, $\kappa$ and $\tau$ to find a lower bound
  for $|\mathbb{S}|$, noting that $k = 4 r$ ensures that
  $\tau \leq \frac{\kappa}{8}$:
  \begin{equation*}
    |\mathbb{S}| \geq F \bigl( m, k, \frac{r}{2} \bigl) \geq m^{r/2} k^r \frac{1}{4^{r/2}} \implies
    |\mathbb{S}| = \Omega(m^{r/2} k^r 2^{-r}).
  \end{equation*}
\end{proof}


\begin{proof}[Proof of \cref{lem:toeplitz:num_sigs}]
  Recall that for a triple $S \in \mathbb{S}$ we define the signature
  as the set of columns and diagonals touched by $S$. Furthermore,
  viewing a triple $(i, j, h) \in ([m] \times [k] \times [k])$ as the
  two entries $(i, j)$ and $(i, h)$ in an $m \times k$ matrix, we
  define the left endpoint as $(i, \min\{j, h \})$ and the right
  endpoint as $(i, \max\{j, h\})$.

  The claim to prove is
  \begin{equation*}
    B = \bigO\Big(\binom{m + k}{r/2} k^{r/2} \binom{k}{r} \Big).
  \end{equation*}
  This is proven by first showing an upper bound on the number of
  choices for the diagonals of left endpoints, then diagonals of right
  endpoints and finally for columns.

  In an $m \times k$ matrix there are $m + k$ different diagonals and
  as the chosen diagonals have to be distinct, there are
  $\binom{m + k}{r/2}$ choices for the diagonals corresponding to left
  endpoints in a triple.

  As the right endpoint of a triple has to be in the same row as the
  left endpoint, there are at most $k$ choices for the diagonal
  corresponding to the right endpoint when the left endpoint has been
  chosen (which it has in our case). This gives a total of $k^{r/2}$
  choices for diagonals corresponding to right endpoints.

  Finally, there are $k$ columns to choose from and the chosen columns
  have to be distinct, and so the total number of choices of columns
  is $\binom{k}{r}$.

  The product of these numbers of choices gives the upper bound
  sought.
\end{proof}

\subsection{Upper Bounding \texorpdfstring{$\Gamma_{2r}$}{\Gamma{}2r}}
\label{sec:toeplitz:upper-bound-t}

\begin{proof}
  Recall that $\Gamma_{2r}$ is defined as the number of tuples
  $S \in ([m] \times [k] \times [k])^{2r}$ such that
  $\forall (i,j,h) \in S$ we have $h \neq j$ and furthermore:
  \begin{itemize}
  \item For all columns $a \in [k]$,
    $\abs[\big]{\{(i,j,h) \in S \mid j=a \vee h=a\}} \mod 2 = 0$;
  \item For all diagonals $a \in \{-(m-1),\dots,k-1\}$,
    $\abs[\big]{\{(i,j,h) \in S \mid j-i=a \vee h-i=a\}} \mod 2 = 0$.
  \end{itemize}

  Let $\mathcal{F} \subseteq ([m] \times [k] \times [k])^{2r}$ be the
  family of tuples satisfying these conditions, and so
  $|\mathcal{F}| = \Gamma_{2r}$.

  To prove an upper bound on $\Gamma_{2r}$, we show how to encode a
  tuple $S \in \mathcal{F}$ using at most
  $r \lg m + 2r \lg k + 2r \lg r + \bigO(r)$ bits, such that $S$ can
  be decoded from this encoding. Since any $S \in \mathcal{F}$ can be
  encoded using $r \lg m + 2r \lg k + 2r \lg r + \bigO(r)$ bits and
  $|\mathcal{F}| = \Gamma_{2r}$, we can conclude:
  \begin{equation*}
    \Gamma_{2r} = 2^{r \lg m + 2r \lg k + 2r \lg r + \bigO(r)}
    = m^{r} k^{2r} r^{2r} 2^{\bigO(r)}.
  \end{equation*}

  Let $\sigma$ denote the encoding function and $\sigma^{-1}$ denote
  the decoding function. If $S \in \mathcal{F}$ and $t \in S$,
  $\sigma(t)$ denotes the encoding of the triple $t$, $\sigma(S)$
  denotes the encoding of the entire tuple $S$, and
  $\sigma(\mathcal{F})$ denotes the image of $\sigma$.

  A tuple $S \in \mathcal{F}$ consists of triples
  $t_1, t_2, \dotsc, t_{2r}$ such that
  $S = t_1 \circ t_2 \circ \dotsb \circ t_{2r}$. To encode
  $S \in \mathcal{F}$ we encode each of the triples and store them in
  the same order:
  $\sigma(S) = \sigma(t_1) \circ \sigma(t_2) \circ \dotsb \circ
  \sigma(t_{2r})$.

  We will first describe a graph view of a tuple $S$ which will be
  useful for encoding and decoding, then we will show an encoding
  algorithm and finally a decoding algorithm.

  \paragraph{Graph}

  A tuple $S \in \mathcal{F}$ forms a (multi)graph structure, where
  every triple $(i, j, h) \in S$ is a vertex. Since
  $S \in \mathcal{F}$, an even number of triple endpoints lie
  on each diagonal. We can thus pair endpoints lying on the same
  diagonal, such that every endpoint is paired with exactly one other
  endpoint. When two triples have endpoints that are paired, the
  triples have an edge between them in the graph. As every triple has
  two endpoints, every vertex has degree two, and so the graph
  consists entirely of simple cycles of length at least two.

  \paragraph{Encoding}

  To encode an $S \in \mathcal{F}$, we first encode each cycle by
  itself by defining the $\sigma(t)$'s for the triples $t$ in the
  cycle. After this, we order the defined $\sigma(t)$'s as the $t$'s
  were ordered in the input.
  \begin{enumerate}
  \item For each cycle we perform the following.
    \begin{enumerate}
    \item We pick any vertex $t = (i, j, h)$ of the cycle and give it
      the type \textsf{head}. Define $\sigma(t)$ as the concatenation
      of its type \textsf{head}, its row $i$, and two columns $j$ and
      $h$. This uses $\lg m + 2 \lg k + \bigO(1)$ bits.
    \item We iterate through the cycle starting after \textsf{head}
      and give vertices, except the last, type \textsf{mid}. The last
      vertex just before \textsf{head} is given the type
      \textsf{last}.
    \item For each triple $t$ of type \textsf{mid} we store its type
      and two columns explicitly. However, instead of storing its row
      we store the index of its predecessor in the cycle order as well
      as how they are connected: If we typed $t_a$ just before $t_b$
      when iterating through the cycle, when encoding $t_b$ we store
      $a$ as well as whether $t_a$ and $t_b$ are connected by the left
      or right endpoint in $t_a$ and left or right endpoint in
      $t_b$. So define $\sigma(t)$ as the concatenation of
      \textsf{mid}, the two columns, the predecessor index and how it
      is connected to the predecessor. All in all we spend
      $\lg r + 2 \lg k + \bigO(1)$ bits encoding each \textsf{mid}.
    \item Finally, to encode the triple $t$ which is typed
      \textsf{last}, we define $\sigma(t)$ as the concatenation of its
      type, its predecessors index, how it is connected to its
      predecessor, and the column of the endpoint on the predecessor's
      diagonal. We thus spend $\lg r + \lg k + \bigO(1)$ bits to
      encode a \textsf{last}. However, since $k = 4r$ the number of
      bits per encoded \textsf{last} is equivalent to
      $2 \lg r + \bigO(1)$, which turns out to simplify the analysis
      later.
    \end{enumerate}

    Note that for each triple, the type is encoded in the first 2
    bits\footnote{To encode the type in 2 bits we could use the
      following scheme: $00 = \mathsf{head}$, $01 = \mathsf{mid}$,
      $10 = \mathsf{last}$, and $11$ is unused.} in the encoding of
    the triple. This will be important during decoding.

  \item After encoding all cycles, we order the encoded triples in the
    same order as the triples in the input, and output the
    concatenation of the encoded triples:
    $\sigma(t_1) \circ \sigma(t_2) \circ \dotsb \circ \sigma(t_{2r})$.
  \end{enumerate}

  To analyse the number of bits needed in total, we look at the
  average number of bits per triple inside a cycle. Since all cycles
  have a length of at least two, each cycle has a \textsf{head} and a
  \textsf{last} triple. These two use an average of
  $\frac{\lg m}{2} + \lg k + \lg r + \bigO(1)$ bits. We now claim that
  the number of bits per \textsf{mid} is bounded by this average.
  Recall that we assumed $\sqrt{m} \geq r$ and $k = 4r$:
  \begin{alignat*}{4}
    \sqrt{m} \geq r \implies \frac{\lg m}{2} \geq \lg r \implies \frac{\lg m}{2} + \lg k + \lg r + 2 &\geq \lg r + 2 \lg k \implies \\
    \frac{\lg m}{2} + \lg k + \lg r + \bigO(1) &\geq |\mathsf{mid}|.
  \end{alignat*}

  Since the average of all $2r$ triples is at most
  $\frac{\lg m}{2} + \lg k + \lg r + \bigO(1)$ bits, the number of
  bits for all triples is at most
  $r \lg m + 2r \lg k + 2r \lg r + \bigO(r)$.

  \paragraph{Decoding}
  We now show how to decode any $\sigma(S) \in \sigma(\mathcal{F})$
  such that $\sigma^{-1}(\sigma(S)) = S$.

  First we need to extract the encodings of the individual triples,
  then we decode each cycle, and finally we restore the order of
  triples. The following steps describe this algorithm in more detail.

  \begin{enumerate}
  \item We first extract the individual $\sigma(t_i)$'s, by iterating
    through the bit string $\sigma(S)$. By looking at the type in the
    first two bits of an encoded triple we know the length of the
    encoded triple. From this we can extract $\sigma(t_i)$ as well as
    know where $\sigma(t_{i+1})$
    begins. \label{item:toeplitz:decoding_extract_encoded_triples}
  \item We now wish to decode each cycle to get the row and two
    columns of every triple, so do the following for each
    \textsf{head} $t_i$.
    \begin{enumerate}
    \item $t \leftarrow t_i$
    \item Look at the first two bits in $t$ to determine its type, and
      while $t$ is not a \textsf{last}, do the following.
      \begin{enumerate}
      \item If $t$ is a \textsf{head}, the row and two columns are
        stored explicitly.
      \item If $t$ is a \textsf{mid}, the two columns are stored
        explicitly. From the reference to $t$'s predecessor, we can
        calculate the diagonal shared between $t$ and its
        predecessor. This can be done as we have stored which endpoint
        (left or right) of the predecessor lies on the diagonal, and
        as we decode in the same order as we encoded, we have already
        decoded the predecessor.

        We know which of $t$'s columns the shared diagonal intersects,
        and so we can calculate the row.

        If the predecessor is a \textsf{head}, take note of which
        diagonal is shared with it.
      \item $t \leftarrow$ the triple that has $t$ as its predecessor.
      \end{enumerate}
    \item $t$ is now a \textsf{last}, so we know the predecessor's
      index as well as a column. However, as the graph consists of
      cycles rather than just linked lists, \textsf{last} shares a
      diagonal with its predecessor as well as sharing a diagonal with
      \textsf{head}. We have noted which diagonal has already been
      shared between \textsf{head} and \textsf{head}'s successor, so
      the other diagonal \textsf{head} touches is shared with
      \textsf{last}.

      From these two diagonals and the column, it is possible to
      calculate the row by the intersection between the predecessor
      diagonal and the column, and to calculate the other column by
      the intersection of the row and the other diagonal.
    \end{enumerate}
  \item Finally order the triples as they were, when the encoded
    versions were extracted in
    step~\ref{item:toeplitz:decoding_extract_encoded_triples}.
  \end{enumerate}
\end{proof}

\section{Lower Bound for \texorpdfstring{$n$}{n} Vectors}
\label{sec:toeplitz:lower-bound-n-vectors}
In this section, we generalise the result of
\cref{sec:toeplitz:onevector} to obtain a lower bound for preserving all
pairwise distances amongst a set of $n$ vectors. Our proof uses
\cref{thm:toeplitz:main} as a building block. Recall that
\cref{thm:toeplitz:main} guarantees that there is a vector $x$ such that
\begin{equation*}
  \Pr\bigg[\Big|\big\|\frac{1}{\sqrt{m}}TDx\big\|_2^2 - 1\Big| > \eps\bigg] \geq 2^{-\bigO(\eps \sqrt{m})}.
\end{equation*}
Moreover, the vector $x$ has non-zeroes only in the first
$\bigO(\sqrt{m})$ coordinates. From such a vector $x$, define
$x_{\rightarrow i}$ as the vector having its $j$'th coordinate equal
to the $(j-i)$'th coordinate of $x$ if $j>i$ and otherwise its $j$'th
coordinate is $0$. In words, $x_{\rightarrow i}$ is just $x$ with all
coordinates \emph{shifted} by $i$.

For $i \leq n-\bigO(\sqrt{m})$ ($i$ small enough that all the non-zero
coordinates of $x$ stay within the $n$ coordinates), we see that $TDx$
and $TDx_{\rightarrow i}$ have the exact same
distribution. Furthermore, if $i \geq m + C\sqrt{m}$ for a big enough
constant $C$, $TDx$ and $TDx_{\rightarrow i}$ are independent. To see
this, observe that the only random variables amongst
$t_{-(m-1)},\dots,t_{d-1}$ that are multiplied with a non-zero
coordinate of $x$ in $TDx$ are
$t_{-(m-1)},\dots,t_{\bigO(\sqrt{m})}$. Similarly, only random
variables $\sigma_1, \dotsc, \sigma_{\bigO(\sqrt{m})}$ amongst $\sigma_1, \dotsc, \sigma_{d}$
are multiplied by a non-zero coordinate of $x$. For
$x_{\rightarrow i}$, the same is true for variables
$t_{-(m-1)+i}, \dotsc, t_{\bigO(\sqrt{m})+i}$ and
$\sigma_{i+1}, \dotsc, \sigma_{i+\bigO(\sqrt{m})}$. If $i \geq m + C\sqrt{m}$,
these sets of variables are disjoint and hence $TDx$ and
$TDx_{\rightarrow i}$ are independent.

With the observations above in mind, we now define a set $X$ of
vectors as follows
\begin{equation*}
  X \eqdef \{\textbf{0}, x, x_{\rightarrow m + C\sqrt{m}}, x_{\rightarrow 2(m+C\sqrt{m})},
  \dotsc, x_{\rightarrow \lfloor (d-C\sqrt{m})/(m+C\sqrt{m})\rfloor (m + C\sqrt{m})}\}.
\end{equation*}

The $\textbf{0}$-vector clearly maps to the $\textbf{0}$-vector when
using $\frac{1}{\sqrt{m}}TD$ as embedding. Furthermore, by the
arguments above, the embeddings of all the remaining vectors are
independent and have the same distribution. It follows that
\begin{align*}
  \Pr\bigg[\forall x_{\rightarrow i} \in X :
  \Big|\big\|\frac{1}{\sqrt{m}}TDx_{\rightarrow i}\big\|_2^2 - 1\Big|
  \leq \eps\bigg] &\leq \big(1 - 2^{-\bigO(\eps \sqrt{m})}\big)^{|X|} \\
                  &\leq \exp\big(-|X|2^{-\bigO(\eps \sqrt{m})}\big). \\
\end{align*}

Now since $\textbf{0} \in X$, it follows that to preserve all pairwise
distances amongst vectors in $X$ to within $(1 \pm \eps)$, we also
have to preserve all norms to within $\pm \eps$. This is true since
for all $x$ of unit norm:
\begin{equation*}
  \Big|\big\|\frac{1}{\sqrt{m}}TDx\big\|_2^2 - 1 \Big|
  = \Big|\big\|\frac{1}{\sqrt{m}}TDx - \frac{1}{\sqrt{m}}TD\textbf{0}\big\|_2^2
    - \big\|x - \zerovector \big\|_2^2\Big|.
\end{equation*}
This proves the following.

\begin{theorem}\label{thm:toeplitz:mainMany}
  Let $T$ be $m \times d$ Toeplitz and $D$ $d \times d$ diagonal. If
  $t_{-(m-1)},t_{-(m-2)}, \dotsc, t_{d-1}$ and
  $\sigma_1, \dotsc, \sigma_d$ are independently distributed
  Rademacher random variables, then for all $0 < \eps < C$, where $C$
  is a universal constant,
  there exists a
  set $X \subset \R^d$ of $n = \Omega(d/m)$ vectors such that
  \begin{equation*}
    \Pr\bigg[\forall x, y \in X :
      \Big|\big\|\frac{1}{\sqrt{m}}TDx
          - \frac{1}{\sqrt{m}}TDy\big\|_2^2
        - \big\| x - y \big\|_2^2\Big|
      \leq \eps\big\| x - y \big\|_2^2 \bigg]
    \leq \exp\big({-n}2^{-\bigO(\eps \sqrt{m})}\big).
  \end{equation*}
\end{theorem}

It follows from \cref{thm:toeplitz:mainMany} that if we want to have
constant probability of successfully embedding \emph{any} set of $n$
vectors, then either it must be the case that $m = \Omega(d/n)$, or
\begin{align*}
  {-n} 2^{-\bigO(\eps \sqrt{m})} &\geq -C_0,
\end{align*}
where $C_0$ is a constant. This in turn implies that
\begin{alignat*}{3}
  \lg n - \bigO(\eps \sqrt{m}) &\leq \lg C_0 &&\implies \\
  \sqrt{m} &= \Omega(\eps^{-1} \log n) &&\implies \\
  m &= \Omega(\eps^{-2}\log^2 n).
\end{alignat*}
This completes the proof of \cref{thm:toeplitz:highlevelmainMany}.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "thesis"
%%% End:
