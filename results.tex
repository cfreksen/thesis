\chapter{Results}
\label{cha:results}

After the overview of Johnson--Lindenstrauss (and multiplication)
results given in \cref{cha:introduction}, let us in this chapter
reiterate and expand upon those I have been a coauthor on, before
showing how we achieved these results in the subsequent chapters.

\section{Toeplitz}
\label{sec:results:toeplitz}

The chronologically first results relate themselves to the Toeplitz
construction~\citep{Freksen:2017:oUTaCMfJLT}. Here the main result is
that the target dimension upper bound of
$m = \bigO(\eps^{-2} \log^2 \frac{1}{\delta})$ from
\citep{Vybiral:2011:aVotJLLfCM} is tight in the worst case, which is
stated formally in \cref{thm:toeplitz:highlevelmain}.

\begin{reptheorem}{thm:toeplitz:highlevelmain}[\citet*{Freksen:2020:oUTaCMfJLT}]
  Let $d, m \in \Npos$ be the source and target dimension with $m < d$
  and let $T \in \{-1, 1\}^{m \times d}$ and
  $D \in \{-1, 0, 1\}^{d \times d}$ be the Toeplitz and diagonal
  matrix in the JLD proposed in \citep{Hinrichs:2011:JLLfCM}. For all
  distortions $0 < \eps < C$, where $C$ is a universal constant, and
  any desired error probability $\delta > 0$, if the following holds
  for every unit vector $x \in \R^d$:
  \begin{equation*}
    \Pr\bigg[\Big|\big\|\frac{1}{\sqrt{m}}TDx\big\|_2^2 - 1\Big| \leq
    \eps\bigg] \geq 1 - \delta,
  \end{equation*}
  then it must be the case that
  $m = \Omega(\eps^{-2} \log^2 \frac{1}{\delta})$.
\end{reptheorem}

The contrapositive interpretation of \cref{thm:toeplitz:highlevelmain}
is that if $m = o(\eps^{-2} \log^2 \frac{1}{\delta})$ then there
exists some hard instance $x \in \R^d$ such that the Toeplitz
construction ``fails'' the distributional Johnson--Lindenstrauss lemma
(\cref{thm:distjl}). As the distributional Johnson--Lindenstrauss
lemma is stronger than the set-based Johnson--Lindenstrauss lemma
(\cref{thm:jl}) one might hope to circumvent the target dimension
lower bound of \cref{thm:toeplitz:highlevelmain}; perhaps one could
prove that the Toeplitz construction could generate a JLT for a set
$X \subset \R^d$ without having to union bound \cref{thm:distjl} over
all pairs $(x, y) \in \binom{X}{2}$ with $\delta = 1/|X|^2$. However,
based on \cref{thm:toeplitz:highlevelmain} we are in
\cref{thm:toeplitz:highlevelmainMany} able to, for many parameters,
construct a set $X \subset \R^d$ of hard instances which brings a
$m = \Omega(\eps^{-2} \log^2 |X|)$ lower bound to the
non-distributional setting.

\begin{reptheorem}{thm:toeplitz:highlevelmainMany}[\citet*{Freksen:2020:oUTaCMfJLT}]
  Let $d, m \in \Npos$ be the source and target dimension with
  $m < d$, let $n \in \Npos$ be the set size, and let
  $T \in \{-1, 1\}^{m \times d}$ and $D \in \{-1, 0, 1\}^{d \times d}$
  be the Toeplitz and diagonal matrix in the JLD proposed in
  \citep{Hinrichs:2011:JLLfCM}. For all distortions $0 < \eps < C$,
  where $C$ is a universal constant, if the following holds for every
  set of $n$ vectors $X \subset \R^d$:
  \begin{equation*}
    \Pr\bigg[\forall x,y \in X : \Big|\big\|\frac{1}{\sqrt{m}}TDx -
          \frac{1}{\sqrt{m}}TDy\big\|_2^2  - \| x-y\|_2^2\Big|  \leq
          \eps\|x-y\|_2^2 \bigg] = \Omega(1),
  \end{equation*}
  then it must be the case that either $m = \Omega(\eps^{-2} \log^2 n)$
  or $m = \Omega(d/n)$.
\end{reptheorem}

So if $m = o(d / n)$ and $m = o(\eps^{-2} \log^2 n)$ there exists a
hard instance $X \subset \R^d$ of size $n$ such that the Toeplitz
construction ``fails'' \cref{thm:jl}.

\section{Feature Hashing}
\label{sec:results:fh}

The next set of results are related to feature
hashing~\citep{Freksen:2018:FUtHT}, where we give a tight tradeoff
between the target dimension $m$, the distortion $\eps$, the error
probability $\delta$, and $\|x\|_\infty / \|x\|_2$. Recall that the
central inequality (\cref{eq:distjl}) in \cref{thm:distjl} is
\begin{equation*}
  \Pr_{f \sim \mathcal{F}}\Bigl[ \, \bigl| \,\|f(x)\|_2^2 - \|x\|_2^2\, \bigr| \leq \eps \|x\|_2^2 \,\Bigr] \geq 1 - \delta .
  \tag{\ref{eq:distjl} restated}
\end{equation*}

Let $\nu_{\mathsf{FH}}(m, \eps, \delta)$ be defined as the maximum
$\nu \in [0, 1]$ such that \cref{eq:distjl} holds with parameters $m$,
$\eps$, and $\delta$ for every $x \in \R^d$ that satisfies
$\|x\|_\infty \leq \nu\|x\|_2$. The tradeoff we prove can then be
formalised as done in \cref{thm:fh:main}.

\begin{reptheorem}{thm:fh:main}[\citet*{Freksen:2018:FUtHT}]
  There exist constants $C \geq D > 0$ such
  that for every $\eps, \delta \in (0,1)$ and
  $m \in \Npos$ the following holds. If
  $\frac{C \lg \frac{1}{\delta}}{\eps^2} \leq m <
  \frac{2}{\eps^2 \delta}$ then
  \begin{equation*}
    \nu_{\mathsf{FH}}(m, \eps, \delta) = \Theta\biggl( \sqrt{\eps} \min\Bigl\{ \frac{\log\frac{\eps m}{\log \frac{1}{\delta}}}{\log\frac{1}{\delta}}, \sqrt{\frac{\log \frac{\eps^2 m}{\log \frac{1}{\delta}}}{\log\frac{1}{\delta}}} \Bigr\} \biggr) .
  \end{equation*}
  Otherwise, if $m \geq \frac{2}{\eps^2 \delta}$ then
  $\nu_{\mathsf{FH}}(m, \eps, \delta) = 1$. Moreover if
  $m < \frac{D\lg\frac{1}{\delta}}{\eps^2}$ then
  $\nu_{\mathsf{FH}}(m, \eps, \delta)=0$.

  Furthermore, some of the hard instances for feature hashing can be
  charactarised by the following proposition. If an $x \in \{0, 1\}^d$
  satisfies $\nu_{\mathsf{FH}} < \|x\|_2^{-1} < 1$ then
  \begin{equation*}
    \Pr_{f \sim \mathsf{FH}}\Bigl[\bigl| \|f(x)\|_2^2 - \|x\|_2^2 \bigr| > \eps\|x\|_2^2 \Bigr] > \delta.
  \end{equation*}
\end{reptheorem}

\Cref{thm:fh:main} shows how the tradeoff behaves asymptotically for
almost\footnote{Note the possible gap between the constants $C$ and
  $D$.} the entire range of parameters and how to construct hard
instances for feature hashing. Note that vectors with the shape
$x = (1, \dotsc, 1, 0, \dotsc, 0)^\T$ are hard instances if they
contain few $1$s.

In addition to the asymptotic analysis shown in \cref{thm:fh:main},
\citep{Freksen:2018:FUtHT} also investigates feature hashing
empirically by estimating the constants appearing and hiding in
\cref{thm:fh:main}. Specifically, for the hard instances described in
\cref{thm:fh:main} we find empirically that whenever
$\frac{4 \lg \frac{1}{\delta}}{\eps^2} \leq m < \frac{2}{\eps^2
  \delta}$,
\begin{equation*}
    \nu_{\mathsf{FH}}(m, \eps, \delta) \geq 0.725 \sqrt{\eps} \min\Bigl\{ \frac{\lg\frac{\eps m}{\lg \frac{1}{\delta}}}{\lg\frac{1}{\delta}}, \sqrt{\frac{\lg \frac{\eps^2 m}{\lg \frac{1}{\delta}}}{\lg\frac{1}{\delta}}} \Bigr\},
\end{equation*}
(except for very sparse vectors, i.e. $\|x\|_0 \le 7$) whereas the
theoretical proof provides a smaller constant $2^{-6}$ in front of
$\sqrt{\eps}$. Since feature hashing satisfies \cref{eq:distjl}
whenever
$\|x\|_\infty \leq \nu_{\mathsf{FH}}(m, \eps, \delta)\|x\|_2$, this
implies that feature hashing works well on even a larger range of
vectors than the we were able to prove. Furthermore, we demonstrate
that the phase transition to $\nu_{\mathsf{FH}} = 1$ is close to
$\frac{2}{\eps^2 \delta}$ as we have run experiments where the
estimated $\eps$ and $\delta$ are such that
$\nu_{\mathsf{FH}}(m, \eps, \delta) < 1$ while
$m = \frac{2 - \gamma}{\eps^2 \delta}$, for some small value $\gamma$.

Finally, the tight bounds on the performance of feature hashing
presented in \cref{thm:fh:main} can be extended to tight performance
bounds for the DKS construction. This extension was not presented in
\citep{Freksen:2018:FUtHT} and is in that sense novel to this
thesis\footnote{However, the ideas in the proof of
  \cref{thm:dks:supersparse} are simple and I would not be surprised
  if they are also found in other works.}. Recall that the DKS
construction, parameterised by a so-called column sparsity
$s \in \Npos$, works by first mapping a vector $x \in \R^d$ to an
$x' \in \R^{sd}$ by duplicating each entry in $x$ $s$ times and then
scaling with $1/\sqrt{s}$, before applying feature hashing to $x'$, as
$x'$ has a more palatable $\ell_\infty / \ell_2$ ratio compared to
$x$. The setting for the extended result is that if we wish to use the
DKS construction but we only need to handle vectors with a small
$\|x\|_\infty / \|x\|_2$ ratio, we can choose a column sparsity
smaller than the usual
$\Theta(\eps^{-1} \log \frac{1}{\delta} \log\frac{m}{\delta})$ and
still get the Johnson--Lindenstrauss guarantees. This is formalised in
\cref{thm:dks:supersparse}, whose proof is included here rather than
in \cref{cha:feature-hashing} so as to let \cref{cha:feature-hashing}
contain only what was published in \citep{Freksen:2018:FUtHT}. The two
pillars of \cref{thm:fh:main} we use in the proof of
\cref{thm:dks:supersparse} is that the feature hashing tradeoff is
tight and that we can force the DKS construction to create hard
instances for feature hashing.

\begin{corollary}
  \label{thm:dks:supersparse}
  Let $\nu_{\mathsf{DKS}} \in [1/\sqrt{d}, 1]$ denote the largest
  $\ell_{\infty} / \ell_2$ ratio required, $\nu_{\mathsf{FH}}$ denote
  the $\ell_{\infty} / \ell_2$ constraint for feature hashing as
  defined in \cref{thm:fh:main}, and $s_{\mathsf{DKS}} \in [m]$ as the
  minimum column sparsity such that the DKS construction with that
  sparsity is a JLD for the subset of vectors $x \in \R^d$ that
  satisfy $\|x\|_\infty / \|x\|_2 \leq \nu_{\mathsf{DKS}}$. Then
  \begin{equation}
    \label{eq:dks:supersparse}
    s_{\mathsf{DKS}} = \Theta\Bigl( \frac{\nu_{\mathsf{DKS}}^2}{\nu_{\mathsf{FH}}^2} \Bigr).
  \end{equation}
\end{corollary}
The upper bound part of the $\Theta$ in \cref{thm:dks:supersparse}
shows how sparse we can choose the DKS construction to be and still
get Johnson--Lindenstrauss guarantees for the data we care about,
while the lower bound shows that if we choose a sparsity below this
bound, there exists vectors who get distorted too much too often
despite having an $\ell_\infty / \ell_2$ ratio of at most
$\nu_{\mathsf{DKS}}$.

\begin{proof}[Proof of~\cref{thm:dks:supersparse}]
  Let us first prove the upper bound:
  $s_{\mathsf{DKS}} = \bigO\bigl(
  \frac{\nu_{\mathsf{DKS}}^2}{\nu_{\mathsf{FH}}^2} \bigr)$.

  Let
  $s \eqdef \Theta\bigl( \frac{\nu_{\mathsf{DKS}}^2}{\nu_{\mathsf{FH}}^2}
  \bigr) \in [m]$ be the column sparsity, and let $x \in \R^d$ be a
  unit vector with $\|x\|_\infty \leq \nu_{\mathsf{DKS}}$. The goal is
  now to show that a DKS construction with sparsity $s$ can embed $x$
  while preserving its norm within $1 \pm \eps$ with probability at
  least $1 - \delta$ (as defined in \cref{thm:distjl}). Let
  $x' \in \R^{sd}$ be the unit vector constructed by duplicating each
  entry in $x$ $s$ times and scaling with $1/\sqrt{s}$ as in the DKS
  construction. We now have
  \begin{equation}
    \label{eq:dks_cor:xprime_infty_upper}
    \|x'\|_\infty \leq \frac{\nu_{\mathsf{DKS}}}{\sqrt{s}} = \Theta(\nu_{\mathsf{FH}}).
  \end{equation}

  Let $\mathsf{DKS}$ denote the JLD from the DKS construction with
  column sparsity $s$, and let $\mathsf{FH}$ denote the feature
  hashing JLD. Then we can conclude
  \begin{align*}
    \Pr_{f \sim \mathsf{DKS}}\Bigl[ \bigl| \|f(x)\|_2^2 - 1 \bigr| \leq \eps \Bigr]
    &= \Pr_{g \sim \mathsf{FH}}\Bigl[ \bigl| \|g(x')\|_2^2 - 1 \bigr| \leq \eps \Bigr]
      \geq 1 - \delta,
  \end{align*}
  where the inequality is implied by \cref{eq:dks_cor:xprime_infty_upper,thm:fh:main}.

  Now let us prove the lower bound:
  $s_{\mathsf{DKS}} = \Omega\bigl(
  \frac{\nu_{\mathsf{DKS}}^2}{\nu_{\mathsf{FH}}^2} \bigr)$.

  Let
  $s \eqdef o\bigl( \frac{\nu_{\mathsf{DKS}}^2}{\nu_{\mathsf{FH}}^2}
  \bigr)$, and let
  $x = (\nu_{\mathsf{DKS}}, \dotsc, \nu_{\mathsf{DKS}}, 0, \dotsc,
  0)^\T \in \R^d$ be a unit vector. We now wish to show that a DKS
  construction with sparsity $s$ will preserve the norm of $x$ to
  within $1 \pm \eps$ with probability strictly less than
  $1 - \delta$. As before, define $x' \in \R^{sd}$ as the unit vector
  the DKS construction computes when duplicating every entry in $x$
  $s$ times and scaling with $1/\sqrt{s}$. This gives
  \begin{equation}
    \label{eq:dks_cor:xprime_infty_lower}
    \|x'\|_\infty = \frac{\nu_{\mathsf{DKS}}}{\sqrt{s}} = \omega(\nu_{\mathsf{FH}}).
  \end{equation}

  Finally, let $\mathsf{DKS}$ denote the JLD from the DKS construction with
  column sparsity $s$, and let $\mathsf{FH}$ denote the feature
  hashing JLD. Then we can conclude
  \begin{align*}
    \Pr_{f \sim \mathsf{DKS}}\Bigl[ \bigl| \|f(x)\|_2^2 - 1 \bigr| \leq \eps \Bigr]
    &= \Pr_{g \sim \mathsf{FH}}\Bigl[ \bigl| \|g(x')\|_2^2 - 1 \bigr| \leq \eps \Bigr]
      < 1 - \delta,
  \end{align*}
  where the inequality is implied by
  \cref{eq:dks_cor:xprime_infty_lower,thm:fh:main}, and the fact
  that $x'$ has the shape of an asymptotically worst case instance for
  feature hashing.
\end{proof}

\section{Multiplication}
\label{sec:results:multiplication}

The final set of results are conditional circuit lower bounds for
integer multiplication and binary
shifting~\citep{Afshani:2019:LBfMvNC}. The results are all conditioned
on the same conjecture, namely \cref{con:multiplication:undirected}.
\begin{repconjecture}{con:multiplication:undirected}[Undirected $k$-pairs Conjecture~\citep{Li:2004:NCtCoMUS}]
  The coding rate is equal to the Multicommodity-Flow rate in
  undirected graphs.
\end{repconjecture}

The first circuit model in which we give size lower bounds is very
powerful as it allows for unbounded circuit depth and for arbitrary
gates as long as the in and out degree of each gate is bounded by some
constant. In \cref{thm:multiplication:LB} we show that computing
multiplication in this model requires circuits of size\footnote{Here a
  circuit's size is its number of wires, which is asymptotically equal
  to its number of gates as the degree is bounded.} $\Omega(n \log n)$
by proving \cref{thm:multiplication:shiftLB}, which states that just
left shifting a bit string a variable amount also requires a circuit
of size $\Omega(n \log n)$.

\begin{reptheorem}{thm:multiplication:LB}[\citet*{Afshani:2019:LBfMvNC}]
  Assuming \cref{con:multiplication:undirected}, every boolean circuit with
  arbitrary gates and bounded in and out degrees that computes the
  product of two numbers given as two $n$-bit strings has size
  $\Omega(n \log n)$.
\end{reptheorem}

\begin{reptheorem}{thm:multiplication:shiftLB}[\citet*{Afshani:2019:LBfMvNC}]
  Assuming \cref{con:multiplication:undirected}, every boolean circuit with
  arbitrary gates and bounded in and out degrees that computes the
  shift problem has size $\Omega(n \log n)$.
\end{reptheorem}

Note that lower bounds in this model also gives number-of-wires lower
bounds in the circuit model where we do not require that the out
degree is bounded. To see this, note that we can replace the outgoing
wires from a gate $g$ with a high out degree, with a constant arity
tree rooted at $g$ that simply broadcasts the output of $g$ to its
leaves. This would only incur a constant factor overhead in the number
of wires. And so a $o(n \log n)$ left-shifting circuit in the
out-degree-unbounded model would result in a $o(n \log n)$
left-shifting circuit in the out-degree-bounded model which
\cref{thm:multiplication:shiftLB} states is impossible assuming
\cref{con:multiplication:undirected}.

Finally, we prove similar results in another circuit model, where the
the gates are still allowed to be arbitrary, but the depth is bounded
to 3. The structure is such that the input and output layer each have
$n$ gates, whereas the middle layer has $\eps n$ gates for some
$\eps \in (0, 1)$. The gates in the middle layer are wired to all
input and output gates, but only at most $c$ input gates are directly
wired to any output gate. \Cref{thm:multiplication:LBdepth3} gives a
lower bound on $c$, and therefore the complexity, for circuits
computing multiplication in this model.

\begin{reptheorem}{thm:multiplication:LBdepth3}[\citet*{Afshani:2019:LBfMvNC}]
  Let $C$ be a depth $3$ circuit that computes the product of two
  numbers given as two $n$-bit strings such that the following holds.
  \begin{enumerate}
  \item The number of gates in the second layer of $C$ is at most
    $\eps n$ for $\eps \leq 1/300$; and
  \item for every output gate $y$ of $C$, the number of input gates
    directly connected to $y$ is at most $c$.
  \end{enumerate}
  Then assuming \cref{con:multiplication:undirected},
  $c = \Omega\bigl(\frac{\log n}{\log \log n}\bigr)$.
\end{reptheorem}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "thesis"
%%% End:
