\section{Feature Hashing}

\title[Feature Hashing]{Fully Understanding the Hashing Trick}

\author[Freksen, Kamma, \& Larsen]{
  \alert{Casper Benjamin Freksen}\inst{1}, Lior Kamma\inst{1}, Kasper Green Larsen\inst{1}
}
\institute[Aarhus University]{
  \inst{1}%
  Department of Computer Science\\
  Aarhus University, Denmark
}

\date{}

\begin{frame}
  \titlepage
\end{frame}

% What is FH

% State theory thm

% State empi result

% Some of proof; down to combinatorics

% Pretty plots from programming

\begin{frame}
  \frametitle{Feature Hashing Definition}
  Feature Hashing is an extremely sparse JL embedding by
  \cite{Weinberger:2009:FHfLSML}, defined by the matrix
  \begin{equation*}
    a_{ij} =
    \begin{dcases*}
      \sigma_j & if $h(j) = i$ \\
      0 & otherwise
    \end{dcases*}
  \end{equation*}

  Embedding time is $\bigO(\|x\|_0)$.

  \pause

  As the column sparsity
  $s = 1 = o(\eps^{-1} \lg (1 / \delta) \lg^{-1} (1 / \eps))$ we
  cannot get the distributional JL guarantees for all vectors in
  $\R^d$.

  \pause

  Solution: Restrict ourselves to vectors that are not ``too sparse''.
\end{frame}

\begin{frame}
  \frametitle{Definition of ``too sparse''}
  Define $\nu(m, \eps, \delta)$ as the maximum $\nu \in [0, 1]$ such
  that for every $x \in \R^d$, if $\|x\|_\infty \leq \nu\|x\|_2$, then
  distributional JL is satisfied.

  \pause

  If $\nu(m, \eps, \delta) = 0$, then no matter how spread out we
  require the mass in a vector to be, there always exist some vector
  that Feature Hashing cannot embed into $m$ dimensions with
  distortion $\eps$ and error probability $\delta$.

  If $\nu(m, \eps, \delta) = 1$ then no matter how sparse a vector is,
  Feature Hashing can always embed it into $m$ dimensions with
  distortion $\eps$ and error probability $\delta$.
\end{frame}

\begin{frame}
  \frametitle{Main Result}
  \begin{theorem}
    There exists universal constants $C \geq D > 0$ such that for every
    $\eps, \delta \in (0, 1)$, and $m \in \Npos$ the following holds.

    \action<1->{If $m < \frac{D \lg (1/\delta)}{\eps^2}$, then
      $\nu(m, \eps, \delta) = 0$.}

    \action<3->{If
      $\frac{C \lg (1/\delta)}{\eps^2} \leq m < \frac{2}{\eps^2
        \delta}$, then
      \begin{equation*}
        \nu(m, \eps, \delta) = \Theta \Biggl(
        \sqrt{\eps} \min \biggl\{
        \frac{\lg \frac{\eps m}{\lg \frac{1}{\delta}}}
        % ---------------------
        {\lg \frac{1}{\delta}},
        \sqrt{
          \frac{\lg \frac{\eps^2 m}{\lg \frac{1}{\delta}}}
          % ---------------------
          {\lg \frac{1}{\delta}}
        }
        \biggr\}
        \Biggr).
      \end{equation*}}
    \action<2->{If $\frac{2}{\eps^2 \delta} \leq m$, then
      $\nu(m, \eps, \delta) = 1$.}
  \end{theorem}

  \action<4->{We will now prove part of a subcase of this.}
\end{frame}

\subsection{Bounding $\nu(m, \eps, \delta)$}

\begin{frame}
  \frametitle{Definitions}
  For every $m,r,k > 0$:

  Define $x^{(k)}$ as the unit vector where the first $k$ entries are
  $1/\sqrt{k}$ and the rest are $0$.

  \pause

  Define $Z_{kr}$ as $\bigl| \|Ax^{(k)}\|_2^2 - 1\bigr|^r$.

  \pause

  \begin{equation*}
    \Lambda(m, r, k) =
    \begin{dcases*}
      \sqrt{\frac{r}{m}} &  if $k \geq mr$ \\
      \max\Bigl\{\sqrt{\frac{r}{m}}, \frac{r^2}{k\ln^2\bigl(\frac{emr}{k}\bigr)}\Bigr\} & if $mr > k \geq \sqrt{mr}$\\
      \max\Bigl\{\sqrt{\frac{r}{m}}, \frac{r^2}{k\ln^2\bigl(\frac{emr}{k}\bigr)}, \frac{r}{k\ln\bigl(\frac{emr}{k^2}\bigr)}\Bigr\} & if $\sqrt{mr} > k$
    \end{dcases*}
  \end{equation*}
\end{frame}

\begin{frame}
  \frametitle{Special Case of Lemma 6 and 7 in the Paper}
  \begin{lemma}
    If $r \leq k$, then there exists constants $C_1, C_2 > 0$ such
    that
    \begin{equation*}
      2^{-C_1} \Lambda(m, r, k) \leq \sqrt[r]{\E [Z_{kr}]} \leq 2^{C_2} \Lambda(m, r, k).
    \end{equation*}
  \end{lemma}

  \pause

  And a few more definitions and parameter choices:
  \begin{align*}
    r &:= \frac{1}{2C_1 + 2C_2 + 5} \lg \frac{1}{\delta}, \\
    t &:= \min \biggl\{
        \frac{\sqrt{e\eps}}{r}\ln\frac{e\eps m}{r},
        \sqrt{\frac{e\eps\ln\frac{e\eps^2 m}{r}}{r}}
        \biggr\}, \\
    k &:= \frac{1}{t^2}.
  \end{align*}
  Note that $\|x^{(k)}\|_\infty = t$, and that $t$ is asymptotically
  the bound in the ``Main Result'' theorem.
\end{frame}

\begin{frame}
  \frametitle{Proof of a Subcase}
  What we will show is the upper bound
  \begin{equation*}
    \nu(m, \eps, \delta) = \bigO \Biggl(
    \sqrt{\eps} \min \biggl\{
    \frac{\lg \frac{\eps m}{\lg \frac{1}{\delta}}}
    % ---------------------
    {\lg \frac{1}{\delta}},
    \sqrt{
      \frac{\lg \frac{\eps^2 m}{\lg \frac{1}{\delta}}}
      % ---------------------
      {\lg \frac{1}{\delta}}
    }
    \biggr\}
    \Biggr),
  \end{equation*}
  assuming
  $\frac{2^{C_2 + 2} \lg (1/\delta)}{\eps^2} \leq m < \frac{2}{\eps^2 \delta}$ and
  $t \leq \frac{1}{\sqrt{r}}$ (i.e.\@ $r \leq k$).

  \pause

  To do this we prove
  $\Pr \Bigl[ \bigl| \|Ax^{(k)}\|_2^2 - 1 \bigr| > \eps \Bigr] \geq
  \delta$.

  \pause

  The first step is proving that $\E[Z_{kr}] \geq 2 \eps^r$ in each of
  the two cases in the $\min$ in the definition of $t$. We will just
  do it for one here.
\end{frame}

\begin{frame}
  \frametitle{Proof of a Subcase, Continued}
  Assume $k = \frac{r^2}{e \eps \ln^2 \frac{e \eps m}{r}}$.

  \pause

  Since $\frac{e \eps m}{r} > e$, then $k < \frac{r^2}{e \eps} \leq \frac{r \lg 1/\delta}{(2C_1 + 2C_2 + 5) e \eps} \leq \frac{2^{C_2 + 2} r \lg 1/\delta}{\eps^2} \leq mr$.

  \pause

  \begin{align*}
    \action<+->{\E[Z_{kr}] &\geq \Lambda(m, r, k)^r \\}
    \action<+->{&\geq \Bigl(\frac{r^2}{k \ln^2 \frac{emr}{k}}\Bigr)^r \\}
    \action<+->{&= \biggl(\frac{e \eps \ln^2 \frac{e \eps m}{r}}
                  {\ln^2 \frac{e^2 \eps m \ln^2 \frac{e \eps m}{r}}{r}}
                  \biggr)^r \\}
    \action<+->{&\geq 2 \eps^r.}
  \end{align*}

  \action<+->{The proof of the other case is similar.}
\end{frame}

\begin{frame}
  \frametitle{Proof of a Subcase, Completed}
  By substituting via $\E[Z_{kr}] \geq 2 \eps^r$ we can apply
  Paley--Zygmund, our lemma, and the fact that
  $\Lambda(m, 2r, k) \leq 4 \Lambda(m, r, k)$:
  \begin{align*}
    \action<+->{\Pr \Bigl[ \bigl| \|Ax^{(k)}\|_2^2 - 1 \bigr| > \eps \Bigr]
    &= \Pr \bigl[ Z_{kr} > \eps^r \bigr] \\}
    \action<+->{&\geq \Pr \bigl[ Z_{kr} > 2^{-1}\E[Z_{kr}] \bigr] \\}
    \action<+->{&\geq \frac{1}{4} \Bigl(
                  \frac{\E^2[Z_{kr}]}{\E[Z_{kr}^2]}
                  \Bigr) \\}
    \action<+->{&\geq \frac{1}{4} \Bigl(
                  \frac{2^{-C_1} \Lambda(m, r, k)}{2^{C_2}\Lambda(m, 2r, k)}
                  \Bigr)^{2r} \\}
    \action<+->{&\geq \delta.}
  \end{align*}

  \action<+->{Therefore $\nu(m, \eps, \delta) \leq \|x^{(k)}\|_\infty = t$.}
\end{frame}

\begin{frame}
  \frametitle{Intuition for $\Lambda$ Lemma}
  Bounding $\sqrt[r]{\E [Z_{kr}]}$ boils down to the following
  problem:

  Given integers $\beta$, $r$, and a set of nodes $V$, how many
  different ways can one add $r$ undirected edges to the nodes such
  that the resulting multigraph has $\beta$ connected compenents of at
  least two nodes, all nodes are in such components, and each
  connected compenent is Eulerian (i.e.\@ can be traversed with an
  Euler tour, i.e.\@ every node has an even degree)?

\end{frame}

\subsection{Empirical Analysis}

\begin{frame}
  \frametitle{Empirical Analysis}
  Vary $m$ and $k$, and generate $2^{24}$ vectors for each with
  $\|x\|_\infty = \|x\|_2/\sqrt{k}$.

  \pause

  Measure distortion $\hat{\eps}$, error probability $\hat{\delta}$,
  and needed $\ell_\infty/\ell_2$ ratio $\hat{\nu}$ on a grid of
  $\eps$ and $\delta$ values.

  \pause

  \begin{align*}
    \hat{\eps}(m, x) &= \frac{\bigl| \|A_mx\|_2^2 - \|x\|_2^2 \bigr|}{\|x\|_2^2} \\
    %
    \hat{\delta}(m, k, \eps)
    &= \frac{\bigl|\{ x : \|x\|_\infty = \|x\|_2/\sqrt{k}, \;
      \hat{\eps}(m, x) \geq \eps \}\bigr|}
    {\bigl|\{ x : \|x\|_\infty = \|x\|_2/\sqrt{k} \}\bigr|} \\
    %
    \hat{\nu}(m, \eps, \delta) &= \max \Bigl\{ \frac{1}{\sqrt{k}}
    : \forall k' \geq k, \; \hat{\delta}(m, k', \eps) \leq \delta \Bigr\}
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Measured $\nu$ vs. Theoretical Bound}
  \hspace*{-\beamerleftmargin}%
  \includegraphics[width=\paperwidth]{../figures/fh/nvt_both_cut_fixed_line}
\end{frame}

\begin{frame}
  \frametitle{Measured $\nu$ vs. Theoretical Bound without $\min$}
  \hspace*{-\beamerleftmargin}%
  \includegraphics[width=\paperwidth]{../figures/fh/nvt_separate_delta_fixed_line}
\end{frame}

\begin{frame}
  \frametitle{Border where Measured $\nu = 1$}
  \hspace*{-\beamerleftmargin}%
  \includegraphics[width=\paperwidth]{../figures/fh/border_1}
\end{frame}

\begin{frame}
  \frametitle{Summary of Feature Hashing Paper}
  Feature Hashing is a very fast dimensionality reduction method used
  successfully in practice.

  General Johnson--Lindenstrauss theoretical guarantee is impossible;
  we need an extra constraint on the input data: $\ell_\infty/\ell_2$
  ratio.

  We give the exact tradeoff between $\eps$, $\delta$, $m$, and
  $\ell_\infty/\ell_2$ to restore the Johnson--Lindenstrauss
  theoretical guarantee for Feature Hashing.
\end{frame}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "presentation"
%%% End:
