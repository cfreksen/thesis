\section{Toeplitz}

\title[Toeplitz JL]{On Using Toeplitz and Circulant Matrices for Johnson--Lindenstrauss Transforms}

\author[Freksen \& Larsen]{
  \alert{Casper Benjamin Freksen}\inst{1}, Kasper Green Larsen\inst{1}
}
\institute[Aarhus University]{
  \inst{1}%
  Department of Computer Science\\
  Aarhus University, Denmark
}

\date{}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Toeplitz?}
  \alt<+>{
    This is (a photograph of) the late Otto Toeplitz

    \includegraphics[height=0.7\textheight]{toeplitz}
  }
  {
    This is a Toeplitz matrix
    \begin{equation*}
      T =
      \begin{pmatrix}
        t_0 & t_1 & t_2 & t_3 & \cdots & t_{d-1} \\
        t_{-1} & t_0 & t_1 & t_2 &\cdots & t_{d-2} \\
        t_{-2} & t_{-1} & t_0 & t_1 &\cdots & t_{d-3} \\
        \vdots & \vdots & \vdots & \vdots & \ddots & \vdots \\
        t_{-(m-1)} & t_{-(m-2)} & t_{-(m-3)} & t_{-(m-4)} & \cdots & t_{d - m}
      \end{pmatrix}
    \end{equation*}
  }

  \pause

  Matrix-vector multiplication is convolution:
  \begin{equation*}
    (Tx)_i = \sum_{j=0}^{d-1} t_{j-i} x_j
  \end{equation*}

\end{frame}

\begin{frame}
  \frametitle{Toeplitz for Johnson--Lindenstrauss}
  Embedding proposed by \cite{Hinrichs:2011:JLLfCM}.

  Let $T$ be a Toeplitz matrix and $D$ be a diagonal matrix with
  i.i.d.~$t_i, d_i \sim \{\pm 1\}$.
  \begin{equation*}
    f(x) = \frac{1}{\sqrt{m}}TDx
  \end{equation*}

  \pause

  Because convolution can be done via FFT, embedding time can be
  reduced to $\bigO(d \lg m)$.

  \pause

  But the best analysis so far \cite{Vybiral:2011:aVotJLLfCM} was only
  able to show it works with $m = \bigO(\eps^{-2} \log^2 (1/\delta))$.

  If Toeplitz could be shown to work with $m = \bigO(\eps^{-2} \log (1/\delta))$
  then it would be the fastest JL embedding known. Would end a decades
  long search for very fast JL embeddings!

\end{frame}

\begin{frame}
  \frametitle{Our Main Result}

  \alt<+>{
    \begin{center}
      \resizebox{!}{0.3\textheight}{\frownie}
    \end{center}
  }
  {\begin{theorem}
      Let $T$ and $D$ be as described in the Toeplitz approach. For all
      $0 < \eps, \delta < 1/2$, if the following holds for every unit
      vector $x \in \R^d$:
      \begin{equation*}
        \Pr\bigg[\Big|\big\|\frac{1}{\sqrt{m}}TDx\big\|_2^2 - 1\Big| <
        \eps\bigg] > 1-\delta,
      \end{equation*}
      then it must be the case that
      $m = \Omega(\eps^{-2} \log^2(1/\delta))$.
    \end{theorem}}

  \pause

  Can be generalised to the problem of preserving the pairwise
  distances of $N$ vectors.

\end{frame}


\subsection{Lower Bound for Toeplitz Matrices}

\begin{frame}
  \frametitle{Main Theorem}
  \begin{theorem}
    Let $T$ and $D$ be as described in the Toeplitz approach. For all
    $0 < \eps < 1/2$, there exists a unit vector $x \in \R^d$ such
    that
    \begin{equation*}
      \Pr\bigg[\Big|\big\|\frac{1}{\sqrt{m}}TDx\big\|_2^2 - 1\Big| > \eps\bigg] \geq 2^{-\bigO(\eps \sqrt{m})}.
    \end{equation*}
  \end{theorem}

  \pause

  This implies our ``Main Result'' theorem as
  \begin{alignat*}{2}
    \delta &\geq 2^{-\bigO(\eps \sqrt{m})} &&\implies \\
    \eps \sqrt{m} &= \Omega(\log(1/\delta)) &&\implies \\
    m &= \Omega(\eps^{-2}\log^2(1/\delta)).
  \end{alignat*}
\end{frame}

\begin{frame}
  \frametitle{Choosing $x$}
  Let $r$ be an even positive integer less than $m/4$ and defined
  later, and $k := 4r$. Now define the input vector, $x$ as having the
  first $k$ coordinates as $1 / \sqrt{k}$ and the rest 0.

  \pause

  Let $Z_r$ is a random variable defined as
  \begin{equation*}
    Z_r := \Big(\big\|\frac{1}{\sqrt{m}} TDx\big\|_2^2 - 1 \Big)^r.
  \end{equation*}

  We are looking for a lower bound on
  \begin{equation*}
    \Pr\big[Z_r^{1/r} > \eps\big].
  \end{equation*}

  Note that $Z_r$ is non-negative.
  \pause

  We use Paley--Zygmund to derive the wanted inequality.
\end{frame}

\begin{frame}
  \frametitle{Paley--Zygmund}
  \begin{theorem}
    If $X$ is a non-negative random
    variable with finite variance and $0 \leq \theta \leq 1$, then
    \begin{equation*}
      \Pr [X > \theta \E[X]] \geq (1 - \theta)^2 \frac{\E^2[X]}{\E[X^2]}.
    \end{equation*}
  \end{theorem}

  \pause

  Choose $1 / 2$ as $\theta$, and $Z_r$ as $X$.

  \pause

  \begin{align*}
    \action<+->{\Pr[Z_r > \E[Z_r]/2] &\geq (1/4)\frac{\E^2[Z_r]}{\E[Z_r^2]}}
                                       \action<+->{\implies \\
    \Pr\bigg[\Big|\big\|\frac{1}{\sqrt{m}} TDx\big\|_2^2 - 1 \Big| > (\E[Z_r]/2)^{1/r}\bigg] &\geq (1/4)\frac{\E^2[Z_r]}{\E[Z_r^2]}}
  \end{align*}

  % \pause does not seem to place nice with \action inside align
  \action<+->{We now need bounds on $\E[Z_r]$ and $\E[Z_r^2]$.}
\end{frame}

\begin{frame}
  \frametitle{Main Theorem, Revisited}
  \begin{lemma}
    If $r = \bigO(\sqrt{m})$, then the random variable $Z_r$ satisfies:
    \begin{flalign*}
      && \E[Z_r] &\geq m^{-r/2} r^r 2^{-\bigO(r)} & \\
      \text{and} && \E[Z_r^2] &\leq m^{-r} r^{2r} 2^{\bigO(r)}. &
    \end{flalign*}
  \end{lemma}

  \pause

  \begin{align*}
    \Pr\bigg[\Big|\big\|\frac{1}{\sqrt{m}} TDx\big\|_2^2 - 1 \Big| >
    (\E[Z_r]/2)^{1/r}\bigg]
    &\geq (1/4)\frac{\E^2[Z_r]}{\E[Z_r^2]} \implies \\
    \Pr\bigg[\Big|\big\|\frac{1}{\sqrt{m}} TDx\big\|_2^2 - 1 \Big| >
    \frac{r}{C_0 \sqrt{m}}\bigg]
    &\geq 2^{-\bigO(r)}.
  \end{align*}

  \pause

  Choose $r$ such that $r / (C_0\sqrt{m}) = \eps$, so $r = \eps C_0 \sqrt{m}$.

\end{frame}

\begin{frame}
  \frametitle{Counting $S$'s}
  Let $\Gamma_r$ denotes the number of tuples
  $S \in ([m] \times [k] \times [k])^r$ such that
  $\forall (i, j, h) \in S$

  \tikzset{
    node style sp/.style={draw,circle,minimum size=1cm},
    node style blk/.style={draw,circle,minimum size=1cm,color=black, fill},
    node style ge/.style={circle,minimum size=1cm}
  }
  \begin{tikzpicture}
    \action<+->{
      \matrix (A) [
      ampersand replacement=\&,
      matrix of math nodes,
      nodes = {node style ge},
      left delimiter  = (,
      right delimiter = )] at (0,0) {
        \phantom{} \& \phantom{} \& \phantom{} \& \phantom{} \& \phantom{} \& \phantom{} \\
        \phantom{} \& \node[node style blk](l){}; \& \phantom{} \& \phantom{} \& \phantom{} \& \node[node style blk](r){}; \& \phantom{} \& \phantom{} \& \phantom{} \\
        \phantom{} \& \phantom{} \& \phantom{} \& \phantom{} \\
        \phantom{} \& \phantom{} \& \phantom{} \& \phantom{} \& \phantom{} \& \phantom{} \& \phantom{} \& \phantom{} \& \phantom{} \\
      };

      \node (i) [left=2 of l] {$i$};
      \node (j) [above=of l] {$j$};
      \node (h) [above=of r] {$h$};
    }

    \action<+->{
      \draw[blue, draw opacity=0.5, line width=1cm, open cap-open cap]
      (A-1-5.135) -- (A-4-8.315);
      \draw[blue, draw opacity=0.5, line width=1cm, open cap-open cap]
      (A-1-1.135) -- (A-4-4.315);

      \draw[red, draw opacity=0.5, line width=1cm, open cap-open cap]
      (A-1-2.north) -- (A-4-2.south);
      \draw[red, draw opacity=0.5, line width=1cm, open cap-open cap]
      (A-1-6.north) -- (A-4-6.south);

      \node () [below=0 of A-4-2] {even};
      \node () [below right=0 of A-4-4.315] {even};
      \node () [below=0 of A-4-6] {even};
      \node () [below right=0 of A-4-8.315] {even};
    }
  \end{tikzpicture}

  \action<+->{
    Then
    \begin{align*}
      \E[Z_r] &= \frac{\Gamma_r}{k^r m^r}, & \E[Z_r^2] = \E[Z_{2r}] &= \frac{\Gamma_{2r}}{k^{2r} m^{2r}}.
    \end{align*}
  }

\end{frame}

\begin{frame}
  \frametitle{Bounding $\Gamma_r$}
  \begin{lemma}
    If $r = \bigO(\sqrt{m})$, then $\Gamma_r$ and $\Gamma_{2r}$ satisfy:
    \begin{flalign*}
      && \Gamma_r &= m^{r/2} k^r r^r 2^{-\bigO(r)} & \\
      \text{and} && \Gamma_{2r} &= m^{r} k^{2r} r^{2r} 2^{\bigO(r)}. &
    \end{flalign*}
  \end{lemma}

  \pause
  \begin{alignat*}{2}
    \E[Z_r] &= \frac{\Gamma_r}{k^r m^r} &&= m^{-r/2} r^r 2^{-\bigO(r)} \\
    \E[Z_r^2] &= \frac{\Gamma_{2r}}{k^{2r} m^{2r}} &&= m^{-r} r^{2r} 2^{-\bigO(r)},
  \end{alignat*}

  which were the bounds we sought.
\end{frame}

\begin{frame}
  \frametitle{Summary of Toeplitz Paper}
  Toeplitz is a very fast dimensionality reduction approach.

  It was an open question whether Toeplitz would embed well enough.

  We proved that Toeplitz cannot embed to the optimal number of
  dimensions, without distorting some inputs too much.
\end{frame}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "presentation"
%%% End:
