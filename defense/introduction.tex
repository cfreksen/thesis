\section{Why, what, and how?}

% Problem

\begin{frame}
  \frametitle{A Scenario}
  Bob likes books and binge reading, and he has noticed that some
  books seem more alike than others and he wonders if books can be
  grouped somehow. ``Maybe it has something to do with that genre
  thing Gene had mentioned? Or perhaps the periods Pete talked
  about?''. Luckily, Bob also has a third friend.

  \pause

  Alice likes algorithms and alliterations, and as she walks home
  after a lecture on unsupervised learning she bumps into Bob, who
  tells her of his problem. ``I know just what you need,'' Alice says,
  ``Clustering!''

  \pause

  ``But what exactly do you mean, when you say that some books seem
  more alike than others?''
\end{frame}

\begin{frame}
  \frametitle{Digging Deeper}
  ``Well, I noticed that some books share a lot of the same words;
  sometimes even sharing small sequences of word. That makes them feel
  very alike to me.''

  After thinking for a bit, Alice has found a solution. ``For each
  book we find the different word sequences of length, say, 3, and we
  count how many there are of each of them.''

  \pause

  ``And when we need to compare two books we just need to find all the
  sequences they share and by how much. Ah, it's just the Euclidean
  distance in a high dimensional real vector space!''

  Bob is not quite sure what those last words meant, but Alice seems
  confident and knowing what to do; a bit like that time she talked
  about using some monoids in the category of endofunctors, whatever
  that is.
\end{frame}

\begin{frame}
  \frametitle{Almost There}
  Just as Alice is about to fire up her favourite TigerLisp-REPL and
  code away, she is reminded of something and asks Bob, ``So, how many
  different words are there?''. ``Well, last time I checked, about 171
  000 in the English dictionary. Is that a problem?''

  \pause

  Alice answered staringly, ``That means we need\dots 171 000 to the
  third\dots 5 000 211 000 000 000 dimensions. That's way too
  much. Are you sure, there are not, like at most 100 words or so?''

  Bob, feeling less hopeful than before, ``No, there aren't. Isn't
  there something we can do?''
\end{frame}

% Lemmas

\begin{frame}
  \frametitle{Johnson--Lindenstrauss, 1984}
  \nocite{Johnson:1984:EoLMiaHS}
  \begin{theorem}[Johnson--Lindenstrauss lemma]
    Let $X \subset \R^d$ be a set of $N$ vectors, then for any
    $0 < \eps < 1/2$, there exists a map $f \colon X \to \R^m$ for
    some $m = \bigO(\eps^{-2} \log |X|)$ such that
    \begin{align*}
      &\forall x, y \in X,\\
      &\quad \bigl| \|f(x) - f(y) \|_2^2 - \|x - y\|_2^2 \bigr| \leq \eps\|x - y\|_2^2.
    \end{align*}
  \end{theorem}

  \pause

  Bound on $m$ is optimal \cite{Larsen:2017:OotJLL}.

  \pause

  Typical properties of $f$: Linear, probabilistic, input oblivious.

\end{frame}

\begin{frame}
  \frametitle{Embedding Single Vector}
  Because of these typical properties, proofs of
  Johnson--Lindenstrauss embeddings typically prove the following
  instead.
  \begin{theorem}[Distributional Johnson--Lindenstrauss lemma]
    For any $0 < \eps, \delta < 1/2$, there exists a distribution
    $\mathcal{F}$ of linear functions $f \colon \R^d \to \R^m$ for
    some $m = \bigO(\eps^{-2} \log \frac{1}{\delta})$ such that
    \begin{align*}
      &\forall x \in \R^d,\\
      &\quad \Pr_{f \sim \mathcal{F}}\Bigl[\bigl| \|f(x)\|_2^2 - \|x\|_2^2 \bigr| \leq \eps \|x\|_2^2 \Bigr] > 1 - \delta.
    \end{align*}
  \end{theorem}

  \pause

  By setting $\delta = \frac{1}{|X|^2}$ and union bounding over all
  $x - y$ for $x, y \in X$, it can be proven that with constant
  probability, all pairwise distances are approximately preserved.
\end{frame}

\begin{frame}[plain]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{bayeux_32}
  \end{center}
\end{frame}

% History (some)

% \item Simplify construction and find constant factor at m
\begin{frame}
  \frametitle{Early Constructions}
  The original construction defines $f(x) = Ax$, where
  $A \in \R^{m \times d}$ is the first $m$ rows of a random orthogonal
  matrix.

  This allows for $m \approx 8 \eps^{-2} \ln |X|$, \cite{Frankl:1990:SGAotBD}.

  \pause

  The entries of $A$ can also be chosen i.i.d. from common
  distributions such as Gaussian \cite{Indyk:1998:ANNTRtCoD},
  Rademacher \cite{Arriaga:1999:aAToLRCaRP}, or indeed any
  sub-Gaussian distribution \cite{Matousek:2008:oVotJLL}.

  \pause

  The time to embed a vector is $\Theta(m\|x\|_0) = \bigO(md)$ for all
  of these.
\end{frame}

% \item Faster embeddings
% \item Sparse: DKS, BlockJL, FH!
\begin{frame}
  \frametitle{Faster JL, Sparsity}
  If $A$ is very sparse (has few nonzero entries: at most $s$ per
  column), computing $Ax$ is fast: $\bigO(s\|x\|_0)$.

  \pause

  The DKS construction chooses $s$ entries per column at random (with
  replacement) and randomly adds or subtracts 1 from each of those
  entries \cite{Dasgupta:2010:aSJLT}.

  This allows for $s = \bigO(\eps^{-1} \log^2 \frac{1}{\delta})$
  \cite{Kane:2010:aDSJLT}.

  \pause

  By choosing the entries without replacement, $s$ can be improved to
  $s = \bigO(\eps^{-1} \log \frac{1}{\delta})$ \cite{Kane:2012:SJLT}.

  \pause

  On the other hand, there exists lower bounds for any sparse
  construction of
  $s = \Omega(\eps^{-1} \log \frac{1}{\delta} / \log \eps^{-1})$
  \cite{Nelson:2013:SLBfDRM}.

\end{frame}

\begin{frame}
  \frametitle{Faster JL, Sparsity, Below the Lower Bound}
  The $s = \Omega(\eps^{-1} \log \frac{1}{\delta} / \log \eps^{-1})$
  is a bound on the worst case input.

  \pause

  Practitioners have found success with much smaller $s$, even $s = 1$!

  \pause

  However, if a vector $x$ we embed is not ``too sparse'', meaning
  $\|x\|_\infty / \|x\|_2 \leq \nu$ for some $\nu$, we can recover the
  theoretical guarantees.

  \pause

  The question is then: How does $\nu$ depend on the construction and
  the other parameters?

  \pause

  To be continued\dots
\end{frame}

% \item FFT-JL: FJLT (+ BCH), RIP, Toeplitz!, Compound (+ KacJL)
\begin{frame}
  \frametitle{Faster JL, Structure}
  An alternative to sparse matrices are structured matrices, where
  computing $Ax$ can be sped up by some algorithm, e.g.\@ FFT.

  \pause

  The FJLT \cite{Ailon:2006:ANNatFJLT} combines the idea of ``not too
  sparse'' ($\nu$) with a preprocessing step, that spreads out the
  mass of a vector.

  $f(x) = PHDx$, where $P$ is a very sparse matrix, $H$ is a
  $d \times d$ Hadamard matrix, and $D$ is a diagonal matrix with
  random signs.

  \pause

  \begin{align*}
    H_2 =
    \begin{pmatrix}
      1 & 1 \\
      1 & -1
    \end{pmatrix}, &&
    H_n =
    \begin{pmatrix}
      H_{n/2} & H_{n/2} \\
      H_{n/2} & -H_{n/2}
    \end{pmatrix}
  \end{align*}

  \pause

  The embedding time for FJLT is
  $\bigO(d \log d + m \log^2 \frac{1}{\delta})$.
\end{frame}

\begin{frame}
  \frametitle{Faster JL, Structure, Continued}
  There are many variations of FJLT, including
  \begin{itemize}
  \item applying several rounds of preprocessing ($HD$) and replacing the
    sparse matrix $P$ with yet another structured matrix \cite{Ailon:2008:FDRuRSoDBCHC},
  \item replacing $PH$ with any matrix satisfying the so-called
    Restricted Isometry Property \cite{Krahmer:2011:NaIJLEvtRIP},
  \item changing the recursive structure of the Hadamard matrix \cite{Liberty:2008:DFRPaLWT}, and
  \item quickly embedding into a suboptimal dimension followed by a
    second and more careful embedding into the optimal dimension \cite{Bamberger:2017:OFJLEfLDS}.
  \end{itemize}

  \pause

  These give various tradeoffs for speed, target dimension and
  parameter requirements. We will focus on one variation: The Toeplitz
  construction.
\end{frame}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "presentation"
%%% End:
