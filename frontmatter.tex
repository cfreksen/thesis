\cleardoublepage
\chapter*{{\Huge Abstract}}
\addcontentsline{toc}{chapter}{Abstract}

During my time as a PhD student, I have published work within two
areas, which will be reflected in this thesis, and so the menu du jour
is a main course consisting of a treatment of so-called
Johnson--Lindenstrauss transforms followed by a dessert of circuit
lower bounds for integer multiplication. BYOB.

After introducing Johnson--Lindenstrauss transforms and establishing
their place as the foundation of many succesful dimensionality
reduction constructions, this thesis provides an overview of the
historical developments and improvements upon the original 1984 paper,
followed by analyses of and insights into the two main flavours of
Johnson--Lindenstrauss transforms illustrated by a tight lower bound
on the \emph{Toeplitz} construction as well as tight a efficiency
tradeoff for the very practical \emph{feature hashing} construction.

On top of showing the exact limits of these constructions, this thesis
demonstrates the powerful techniques used to arrive at these results,
which build upon both central probabilistic theorems as well as on
translating between real valued randomised linear algebra and discrete
combinatorial graph problems. These techniques form a valuable toolbox
for any theoretician seeking a deep understanding of the structure and
behaviour of these widespread and most useful algorithms.

After this hearty serving of linear algebra and probability theory, a
refreshing treat of circuit lower bounds awaits.

Integer multiplication is one of the most fundamental operations
performed by computers today, yet we do not know how efficiently it
can be implemented. Even answers to questions such as ``Is
multiplication fundamentally harder than addition?'' seem to be
outside our reach. This thesis brings us much closer to an answer as
it shows that based on a core conjecture from the network coding
literature, multiplication is indeed harder than addition, and that
the current state-of-the-art in terms of boolean circuits for
multiplication are optimal. Furthermore, it also relates the
conjecture from network coding to general and longstanding conjectures
in circuit complexity.

\cleardoublepage
\chapter*{{\Huge Resum\'e}}
\addcontentsline{toc}{chapter}{Resum\'e}
\label{sec:front:resume}

\begin{otherlanguage}{danish}
  Titlen på denne afhandling kan oversættes til: ``Et kvad om
  Johnson og Lindenstrauss''.

  Gennem min tid som ph.d.-studerende har jeg udgivet artikler inden
  for to forskningsfelter, hvilket afspejles i denne
  afhandling. Dagens menu består således af såkaldte
  Johnson--Lindenstrauss-transformationer som hovedret efterfulgt af
  kredsløbsnedergrænser for udregning af heltalsprodukter til
  dessert. Vinen står De selv for.

  Afhandlingen introducerer først
  Johnson--Lindenstrauss-transformationer og fastslår deres centrale
  plads som grundstenen i flere successrige
  dimensionsreduktionskonstruktioner, hvorefter den gennemgår den
  historiske udvikling, der er sket inden for feltet, siden Johnson og
  Lindenstrauss udgav deres artikel i 1984. Dette efterfølges af
  analyser af de to hovedstrømninger inden for
  Johnson--Lindenstrauss-transformationer eksemplificeret ved en tæt
  nedergrænse for \emph{Toeplitz}-konstruktionen samt en præcis
  ydeevnesammenhæng for den praksisorienterede \emph{feature
    hashing}-konstruktion (egenskabsfordelingskonstruktionen).

  Ud over selve nedergrænserne og sammenhængene fremviser denne
  afhandling også de nyttige bevisteknikker, der banede vej til
  resultaterne, og som bygger på centrale sandsynlighedsteoretiske
  sætninger samt sammenkoblinger mellem tilfældig realtals-lineær
  algebra og diskrete kombinatoriske grafproblemer. Disse
  bevisteknikker viser sig som værdifulde værktøjer, der vil være
  enhver teoretiker værdig, hvis vedkommende vil erhverve sig en
  dybere forståelse af disse anerkendte og aldeles anvendelige
  algoritmer.

  Efter at have nydt en fyldig tallerken med lineær algebra og
  sandsynlighedsteori, venter der en forfriskende lækkerbisken af
  kredsløbsnedergrænser.

  Heltalsproduktsudregning er en af de mest fundamentale og mest
  anvendte operationer udført af datamater i dag, men det er samtidig
  ukendt, hvor effektivt det kan beregnes. Selv spørgsmål såsom: ``Er
  heltalsprodukter fundamentalt sværere at udregne end
  heltalssummer?'' virker uden for vores rækkevidde. Denne afhandling
  bringer os meget tættere på et svar, idet den viser, at antaget en
  central konjektur inden for netværkskodningsteori er
  heltalsprodukter signifikant sværere at udregne end heltalssummer,
  samt at vores nuværende bedste heltalsproduktudregningskredsløb er
  optimale. Endvidere relaterer afhandlingen også konjekturen fra
  netværkskodningsteorien til generelle og mangeårige konjekturer
  inden for kredsløbskompleksitet.
\end{otherlanguage}

\cleardoublepage
\chapter*{{\Huge Acknowledgments}}
\addcontentsline{toc}{chapter}{Acknowledgments}

I owe my thanks to the many people who have supported me and been part
of my journey to reach this point (and beyond!).

First of all I would like to thank my advisor, Kasper Green Larsen,
who has taken me under his quite sizeable wing and guided me for the
last four years. You have introduced me to many of the subjects I have
found so intriguing and rewarding to work on, and you have done so by
pushing me towards higher, more challenging goals while at the same
time giving me space to tackle academic and personal problems on my
own when I needed to. I owe much of what I have learned and grown to
be to you.

The thesis proper and the research it presents could not have been
done without the valuable help of my co-authors: Peyman Afshani, Lior
Kamma, and Kasper Green Larsen. I thank you for the deep and
interesting discussions we had while working together, for the
perspectives we shared and for those we didn't. Thank you for letting
me join you in our quest for \emph{fame}.

I would also like to thank the people I subjected early drafts of this
thesis to: Kasper Green Larsen, Ida Larsen-Ledet, and Mathias
Vorreiter Pedersen. Thank you for your excellent feedback and
suggestions, and for keeping me motivated through this period of
lonesome labour as I compiled and summarised my studies. And I thank
you, on behalf of the reader, for removing many splinters and blunders
from the text that I would not have found on my own, though should any
still remain they are not to blame.

In the autumn semester of 2018 I visited the theoretical computer
science group at Harvard University and I was fortunate enough to be
hosted by Jelani Nelson, who along with the PhD students and postdocs
made that semester an especially cherished part of my studies. I thank
you for welcoming me, letting me be a part of your group, and making
me feel at home so far away from it.

I also wish to thank the PhD and postdoc community at the computer
science department at Aarhus University. It is hard to imagine a
better environment to grow in; a better camaraderie of academics to
belong to. I would especially like to thank Anders Peter Kragh
Dalskov, Ida Larsen-Ledet, and Mathias Vorreiter Pedersen for being
with me from my time as an undergraduate to today and to Svend
Christian Svendsen for being my office mate as a PhD student. You have
all always been there for me when I needed to vent about the trials
and tribulations that have tested me and we have shared in the joy
that is also a part of these studies.

Another community that I owe a lot to is Fredagscaféen: the Friday bar
at the computer science department. I was introduced to the bar in my
first week at the university, and as far as I recall, after having
been there a few times with my new classmates, one Friday where I had
nothing better to do, I went there on my own. Nevertheless, I found
people to talk with (or they found me) who made the young and hesitant
person that I was, feel that I belonged at Aarhus University, and
since then Fredagscaféen and the people surrounding it have been an
anchor for many of my friendships and much of my social life at Aarhus
University.

I wish to thank the people who helped me feel welcome and at home when
I first started at Aarhus University. I wish to thank my tutors: Nick
Bakkegaard, Mikkel Lysholt Robdrup, Mathias Rav, and Mathias Veje,
with Rav pursuing and achieving a PhD in the same research group as
me, allowing me to seek his guidance throughout most of my
studies. And I wish to thank the many lecturers and teaching
assistents that have taught me through the years. I would especially
like to thank Gerth Stølting Brodal and Olivier Danvy for early on
sparking my interest in algorithms and in the structure and
(re)presentation of knowledge, respectively.

I am grateful for my family and friends, who have supported me
throughout my life. I would like to thank my parents and siblings for
an upbringing that made me who I am and equipped me to go on the
journeys my life have consisted of. You mean so much to me.

And finally, I would like to thank to the staff at Aarhus University
Hospital for killing parts of me so that what remains may live a while
longer.

\vspace{2ex}
\begin{flushright}
  \makeatletter\emph{\theauthor,}\makeatother\\
  \emph{Aarhus, \thedate.}
\end{flushright}


\cleardoublepage
\chapter*{{\Huge Preface}}
\addcontentsline{toc}{chapter}{Preface}

This thesis is based on three papers: ``On Using Toeplitz and
Circulant Matrices for Johnson--Lindenstrauss
Transforms''~\citep{Freksen:2017:oUTaCMfJLT, Freksen:2020:oUTaCMfJLT},
``Fully Understanding the Hashing Trick''~\citep{Freksen:2018:FUtHT},
and ``Lower Bounds for Multiplication via Network
Coding''~\citep{Afshani:2019:LBfMvNC}, and so some of text from these
papers appear in the thesis in the following way:

\begin{itemize}
\item \Cref{cha:toeplitz} is a subset of
  \citep{Freksen:2020:oUTaCMfJLT}.
\item \Cref{cha:feature-hashing} is a subset of
  \citep{Freksen:2018:FUtHT}.
\item \Cref{sec:intro:multiplication,cha:multiplication} are subsets
  of \citep{Afshani:2019:LBfMvNC}.
\end{itemize}

The Johnson--Lindenstrauss papers
(\cref{cha:toeplitz,cha:feature-hashing}) have been adjusted so that
the notation is consistent between them and \cref{part:overview}, and
their introductions have been merged, rewritten, and extended in
\cref{cha:introduction} so as to give a cohesive and more detailed
overview to the subject.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "thesis"
%%% End:
