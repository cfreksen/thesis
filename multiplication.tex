\chapter[Multiplication]{Multiplication \\ \textit{\normalsize This chapter is based on~\citep{Afshani:2019:LBfMvNC}}}
\label{cha:multiplication}

As stated in \cref{cha:results}, the main results in
\citep{Afshani:2019:LBfMvNC} are conditional circuit lower bounds for
computing integer multiplication and even binary shifting in two
circuit computational models. The conjecture that these results build
upon is \cref{con:multiplication:undirected}, which states that when
communicating between $k$ pairs of nodes in a network limited edge
capacities, limiting ourselves to multicommodity flow techniques
(forwarding, splitting, and merging messages) is as efficient as
utilising arbitrary computation and message encodings at the nodes of
the network.

\begin{conjecture}[Undirected $k$-pairs Conjecture~\citep{Li:2004:NCtCoMUS}]
  \label{con:multiplication:undirected}
  The coding rate is equal to the Multicommodity-Flow rate in
  undirected graphs.
\end{conjecture}

From this, we can show that even if we allow arbitrary depth and
arbitrary gates, as long as the in and out degree are bounded,
multiplication (\cref{thm:multiplication:LB}) and even left shifting
(\cref{thm:multiplication:shiftLB}) requires the circuit to be of size
$\Omega(n \log n)$.

\begin{theorem}
  \label{thm:multiplication:LB}
  Assuming \Cref{con:multiplication:undirected}, every boolean circuit
  with arbitrary gates and bounded in and out degrees that computes
  the product of two numbers given as two $n$-bit strings has size
  $\Omega(n \log n)$.
\end{theorem}

\begin{theorem}
  \label{thm:multiplication:shiftLB}
  Assuming \cref{con:multiplication:undirected}, every boolean circuit
  with arbitrary gates and bounded in and out degrees that computes
  the shift problem has size $\Omega(n \log n)$.
\end{theorem}


As stated in \cref{sec:intro:multiplication}, the following circuit
model is especially interesting with respect to circuit lower bounds
and some of Valiant's conjectures. In this model, circuits consists of
3 layers, where the input and output layers are only sparsely
connected, and while the gates in the middle layer are fully connected
to all input and output gates, the middle layer only contains a small
fraction of the gates of the
circuit. \Cref{thm:multiplication:LBdepth3} shows a input-output
sparsity lower bound for circuits computing multiplication in this
model.

\begin{theorem}
  \label{thm:multiplication:LBdepth3}
  Let $C$ be a depth $3$ circuit that computes the product of two
  numbers given as two $n$-bit strings such that the following holds.
  \begin{enumerate}
  \item The number of gates in the second layer of $C$ is at most
    $\eps n$ for $\eps \leq 1/300$; and
  \item for every output gate $y$ of $C$, the number of input gates
    directly connected to $y$ is at most $c$.
  \end{enumerate}
  Then assuming \Cref{con:multiplication:undirected},
  $c = \Omega\bigl(\frac{\log n}{\log \log n}\bigr)$.
\end{theorem}

Core concepts such as boolean circuits and multicommodity flow are
formally defined in \cref{sec:prelim}, and after introducing some
useful tools and techniques in \cref{sec:multiplication:tools}, the
proof of \cref{thm:multiplication:shiftLB} and therefore
\cref{thm:multiplication:LB} is given in the short
\cref{sec:multiplication:LB}, while the more involved proof of
\cref{thm:multiplication:LBdepth3} is located in
\cref{sec:multiplication:LBdepth3}.

\section{Preliminaries}
\label{sec:prelim}
We now give a formal definition of Boolean circuits with arbitrary
gates, followed by definitions of the $k$-pairs communication problem
and the multicommodity flow problem. In the two latter problems we
reuse some of the definitions used by \citet{Farhadi:2019:LBfEMISvNC},
which have been simplified a bit compared to the more general
definition by \citet{Adler:2006:otCoIN}. In particular, we have forced
communication networks to be directed acyclic graphs. This is
sufficient to prove our lower bounds and simplifies the definitions
considerably.

\paragraph{Boolean Circuits with Arbitrary Gates.}
A \emph{Boolean Circuit with Arbitrary Gates} with $n$ source or input
nodes and $m$ target or output nodes is a directed acyclic graph $C$
with $n$ nodes of in-degree $0$, which are called \emph{input gates},
and are labeled with input variables $X=\{x_i\}_{i \in [n]}$ and $m$
nodes out-degree $0$, which are called \emph{output gates} and are
labeled with output variables $Y=\{y_i\}_{i \in [m]}$. All other nodes
are simply called \emph{gates}. For every gate $u$ of in-degree
$k \ge 1$, $u$ is labeled with an arbitrary function
$f_u \colon \{0,1\}^k \to \{0,1\}$. The circuit is also equipped with
a topological ordering $v_1, \dotsc, v_t$ of $C$, in which $v_i = x_i$
for $i \in [n]$ and $v_{t-i+1} = y_{m-i+1}$ for all $i \in [m]$. The
\emph{depth} of a circuit $C$ is the length of the longest path
between an input and an output node in $C$.
%
An \emph{evaluation} of a circuit on an $n$ bit input
$x=(x_1, \dotsc, x_n) \in \{0,1\}^n$ is conducted as follows. For
every $i \in [n]$, assign $x_i$ to $v_i$. For every $j \geq n+1$,
assign to $v_j$ the value $f_{v_j}(u_1, \dotsc, u_k)$, where
$u_1, \dotsc, u_k$ are the nodes of $C$ with edges going into $v_j$ in
the order induced by the topological ordering. The \emph{output} of
$C$ on an $n$ bit input $x = (x_1, \dotsc, x_n)$, denoted
$C(x_1, \dotsc, x_n)$ is the value assigned to $(y_1, \dotsc, y_m)$ in
the evaluation. We say a circuit computes a function
$f \colon \{0,1\}^n \to \{0,1\}^m$ if for every
$x=(x_1, \dotsc, x_n) \in \{0,1\}^n$,
$f(x_1, \dotsc, x_n)=C(x_1, \dotsc, x_n)$.

For every $j \in [t]$ and $b \in \{0,1\}$, we \emph{hardwire} $b$ for
$v_j$ in $C$ by removing $v_j$ and all adjacent edges from $C$, and
replacing $v_j$ for $b$ in the evaluation of $f_{v_i}$ for every
$i > j$ such that $(v_j, v_i)$ is an edge in $C$.

\paragraph{$k$-Pairs Communication Problem.}
The input to the $k$-pairs communication problem is a directed acyclic
graph $G = (V, E)$ where each edge $e \in E$ has a capacity
$c(e) \in \Rpos$. There are $k$ sources $s_1, \dotsc, s_k \in V$ and $k$
sinks $t_1, \dotsc, t_k \in V$.

Each source $s_i$ receives a message $A_i$ from a predefined set of
messages $A(i)$. It will be convenient to think of this message as
arriving on an in-edge. Hence we add an extra node $S_i$ for each
source, which has a single out-edge to $s_i$. The edge has infinite
capacity.

A network coding solution specifies for each edge $e \in E$ an
alphabet $\Gamma(e)$ representing the set of possible messages that
can be sent along the edge. For a node $v \in V$, define $\In(u)$ as
the set of in-edges at $u$. A network coding solution also specifies,
for each edge $e = (u, v) \in E$, a function
$f_e \colon \prod_{e' \in \In(u)} \Gamma(e') \to \Gamma(e)$ which
determines the message to be sent along the edge $e$ as a function of
all incoming messages at node $u$. Finally, a network coding solution
specifies for each sink $t_i$ a decoding function
$\sigma_i \colon \prod_{e \in \In(t_i)} \Gamma(e) \to A(i)$. The
network coding solution is correct if, for all inputs
$A_1, \dotsc, A_k \in \prod_i A(i)$, it holds that $\sigma_i$ applied
to the incoming messages at $t_i$ equals $A_i$, i.e.\@ each source
must receive the intended message.

In an execution of a network coding solution, each of the extra nodes
$S_i$ starts by transmitting the message $A_i$ to $s_i$ along the edge
$(S_i, s_i)$. Then, whenever a node $u$ has received a message $a_e$
along all incoming edges $e = (v, u)$, it evaluates
$f_{e'}(\prod_{e \in \In(u)} a_e)$ on all out-edges and forwards the
message along the edge $e'$.

We define the \emph{rate} of a network coding solution as follows: Let
each source receive a uniform random and independently chosen message
$A_i$ from $A(i)$. For each edge $e$, let $A_e$ denote the random
variable giving the message sent on the edge $e$ when executing the
network coding solution with the given inputs. Let $H(\cdot)$ denote
the binary Shannon entropy. The network coding solution achieves rate
$r$ if:
\begin{itemize}
\item $H(A_i) \geq r$ for all $i$.
\item For each edge $e \in E$, we have $H(A_e) \leq c(e)$.
\end{itemize}
The intuition is that the rate is $r$, if the solution can handle
sending a message of entropy $r$ bits between every source-sink pair.

\paragraph{Multicommodity Flow.}
A multicommodity flow problem in an undirected graph $G=(V,E)$ is
specified by a set of $k$ source-sink pairs $(s_i,t_i)$ of nodes in
$G$. We say that $s_i$ is the source of commodity $i$ and $t_i$ is the
sink of commodity $i$. Each edge $e \in E$ has an associated capacity
$c(e) \in \Rpos$.
%
A (fractional) solution to the multicommodity flow problem specifies
for each pair of nodes $(u, v)$ and commodity $i$, a flow
$f^i(u, v) \in [0,1]$. Intuitively $f^i(u, v)$ specifies how much of
commodity $i$ that is to be sent from $u$ to $v$. The flow satisfies
\emph{flow conservation}, meaning that:
\begin{itemize}
\item For all nodes $u$ that is not a source or sink, we have
  $\sum_{w \in V} f^i(u, w) - \sum_{w \in V} f^i(w, u) = 0$.
\item For all sources $s_i$, we have
  $\sum_{w \in V} f^i(s_i, w) - \sum_{w \in V}f^i(w, s_i) = 1$.
\item For all sinks we have
  $\sum_{w \in V} f^i(w, t_i) - \sum_{w \in V} f^i(t_i, w) = 1$.
\end{itemize}
The flow also satisfies that for any pair of nodes $(u, v)$ and
commodity $i$, there is only flow in one direction, i.e.\@ either
$f^i(u,v) = 0$ or $f^i(v,u) = 0$. Furthermore, if $(u, v)$ is not an
edge in $E$, then $f^i(u,v) = f^i(v,u) = 0$. A solution to the
multicommodity flow problem achieves a rate of $r$ if:
\begin{itemize}
\item For all edges $e = (u, v) \in E$, we have
  $r \cdot \sum_i (f^i(u,v) + f^i(v,u)) \leq c(e)$.
\end{itemize}
Intuitively, the rate is $r$ if we can handle a demand of $r$ for
every commodity.

\paragraph{The Undirected $k$-Pairs Conjecture.}
\Cref{con:multiplication:undirected} implies the following for our
setting: Given an input to the $k$-pairs communication problem,
specified by a directed acyclic graph $G$ with edge capacities and a
set of $k$ source-sink pairs, let $r$ be the best achievable network
coding rate for $G$. Similarly, let $G'$ denote the undirected graph
resulting from making each directed edge in $G$ undirected (and
keeping the capacities and source-sink pairs). Let $r'$ be the best
achievable flow rate in $G'$. \Cref{con:multiplication:undirected}
implies that $r \leq r'$.

Having defined coding rate and flow rate formally, we also mention
that a result of \citet{Braverman:2017:CiUGievHonHaa} implies that if
there exists a graph $G$ where the network coding rate $r$, and the
flow rate $r'$ in the corresponding undirected graph $G'$, satisfies
$r \geq (1 + \eps)r'$ for a constant $\eps > 0$, then there exists an
infinite family of graphs $\{G^*\}$ for which the corresponding gap is
at least $(\lg |G^*|)^c$ for a constant $c > 0$. So far, all evidence
suggest that no such gap exists, as formalised in
\cref{con:multiplication:undirected}.

\section{Key Tools and Techniques}
\label{sec:multiplication:tools}
The main idea in the heart of both proofs is the simple fact that in a
graph with $t$ vertices and maximum degree at most $c$, most node
pairs lie far away from one another. Specifically, for every node $u$
in $G$, at least $t - \sqrt{t}$ nodes have distance
$\geq \frac{1}{2}\log_c t$ from $u$. While this key observation is
almost enough to prove \cref{thm:multiplication:shiftLB}, the proof of
\cref{thm:multiplication:LBdepth3} requires a much more subtle
approach, as there is no bound on the maximum degree in the circuits
in question. The only bound we have is on the number of wires going
directly between from input gates into output gates. Specifically,
every two nodes in the underlying undirected graph are at distance
$\leq 3$ (see \cref{fig:multiplication:circuit}).

In order to overcome this obstacle, we present a construction of a
communication network based on the circuit $C$ that essentially
eliminates the middle layer in the depth-$3$ circuit $C$, thus leaving
a bipartite graph with bounded maximum degree.
%
To this end, we observe that since the size of the middle layer is
bounded by $\eps n$, then there exists a large set $\mathcal{F}$ of
inputs in $\{0,1\}^n$ such that on all inputs from $\mathcal{F}$, the
gates $f_1, \dotsc, f_{\eps n}$ attain the same values. By hardwiring
these values to the circuit, we can evaluate the circuit for all
inputs in $\mathcal{F}$ on a depth-$2$ circuit $\Gamma$ obtained from
$C$ by removing $f_1, \dotsc, f_{\eps n}$. We next turn to construct
the communication network.
%
Employing ideas recently presented by \citet{Farhadi:2019:LBfEMISvNC},
we ``wrap'' the depth-$2$ circuit by adding source and target
nodes. In order to cope with inputs that do not belong to
$\mathcal{F}$, we add a designated \emph{supervisor} node $u$ (see
\cref{fig:multiplication:network}). Loosely speaking, the source
nodes transmit their input to $u$, and $u$ sends back the information
needed to ``edit'' the input string $x$ and construct an input string
$x' \in \mathcal{F}$, which is then transferred to the circuit
$\Gamma$ as blackbox.

\paragraph{The Correction Game.}
In order to bound the edge capacities of the network $G$ in a way that
the supervisor node can transmit enough information to achieve a high
communication rate, but then again not allow to much flow to go
through the supervisor when considering $G$ as a multicommodity flow
instance, \citet{Farhadi:2019:LBfEMISvNC} defined a game between a set
of $m$ players and a supervisor, where given a fixed set
$\mathcal{F} \subseteq \{0,1\}^n$ and a random string
$\beta \in \{0,1\}^n$ given as a concatenation of $m$ strings
$\beta_1, \dotsc, \beta_m$ of length $n/m$ each, the goal is to
``correct'' $\beta$ and produce a string $\chi \in \{0,1\}^n$ such that
$\beta \oplus \chi \in \mathcal{F}$. The caveat is that the only
communication allowed is between the players and the supervisor. That
is, no communication, and thus no cooperation, is allowed between the
$m$ players. Formally, the game is defined as follows.

\begin{definition}
  Let $\mathcal{F} \subseteq \{0,1\}^n$. The
  \emph{$\mathcal{F}$-correction game} with $m+1$ players is defined
  as follows. The game is played by $m$ ordinary players
  $p_1, \dotsc, p_m$ and one designated \emph{supervisor} player $u$.
  The supervisor $u$ receives $m$ strings
  $\beta_1, \dotsc, \beta_m \in \{0,1\}^{n/m}$ chosen independently at
  random. For every $\ell \in [m]$, $u$ then sends $p_\ell$ a message
  $R_\ell$. Given $R_\ell$, the player $p_\ell$ produces a string
  $\chi_\ell \in \{0,1\}^{n/m}$ such that
  $(\beta_1 \oplus \chi_1) \circ (\beta_2 \oplus \chi_2) \circ \dotsb
  \circ (\beta_m \oplus \chi_m) \in \mathcal{F}$.
\end{definition}

\citeauthor{Farhadi:2019:LBfEMISvNC} additionally present a protocol
for the $\mathcal{F}$-correction game in which the supervisor player
sends prefix-free messages to the $m$ players, and moreover, they give
a bound on the amount of communication needed as a function of the
number of players and the size of $\mathcal{F}$.
\begin{lemma}[\citep{Farhadi:2019:LBfEMISvNC}]
  \label{l:protocolBound}
  If $|\mathcal{F}| \ge 2^{(1-\eps)n}$, then there exists a protocol
  for the $\mathcal{F}$-correction game with $m+1$ players such that
  the messages $\{R_\ell\}_{\ell \in [m]}$ are prefix-free and
  % $$\sum_{\ell \in [m]}{\E[|R_\ell|]} \leq 3m + 2m\lg\Bigl(\frac{X}{m} + 1\Bigr) + X \lg\Bigl(\frac{n}{X}\Bigr) \;,$$
  \begin{equation*}
    \sum_{\ell \in [m]}{\E[|R_\ell|]} \leq 3m + 2m\lg\Bigl(\sqrt{\frac{\eps}{2}}\cdot \frac{n}{m} + 1\Bigr) + \sqrt{\frac{\eps}{8}}\cdot n \lg\frac{2}{\eps} \;.
  \end{equation*}
  % where $X = \E_{x \in_R\{0,1\}^n}[d(x,\mathcal{F})]$ is the expected Hamming distance of a random point from $\mathcal{F}$.
\end{lemma}
%Finally, they show that if $\mathcal{F}$ is big enough, then a random point cannot be too far from $\mathcal{F}$ in expectation.
%\begin{lemma}[\cite{FHLS18}] \label{l:distance}
%Let $\mathcal{F} \subseteq \{0,1\}^n$ be such that $|\mathcal{F}| \ge 2^{(1-\eps) n}$ for some $\eps \in (0,1)$. Then $$\E_{x \in_R\{0,1\}^n}[d(x,\mathcal{F})] \le \sqrt{\frac{\eps}{2}} \cdot n \;.$$
%\end{lemma}

\section{A Lower Bound for Boolean Circuits Computing Multiplication}
\label{sec:multiplication:LB}
In this section we show that conditioned on
\cref{con:multiplication:undirected}, every bounded degree circuit
computing multiplication must have size at least $\Omega(n \log n)$,
thus proving \cref{thm:multiplication:LB}.
%
In fact, we will prove something slightly stronger. Define the shift
function $s \colon \{0,1\}^n \times [n] \to \{0,1\}^{2n}$ as
follows. For every $x = (x_1, \dotsc, x_n) \in \{0,1\}^n$ and
$\ell \in [n]$, $s(x, \ell) = (y_1, \dotsc, y_{2n})$ where
$y_j = x_{j-\ell+1}$ if $\ell \leq j \leq \ell+n-1$ and $y_j = 0$
otherwise. We will show that every circuit with bounded in and out
degrees that computes the shift function on $n$-bit numbers has size
$\Omega(n \log n)$, thus proving
\cref{thm:multiplication:shiftLB}. Clearly, a circuit that can compute
the product of two $n$-bit numbers can also compute the shift
function. Let $c$ denote the maximum in and out degree in $C$, and let
$j \in [n]$. Then in the undirected graph induced by $C$, there are at
most $\sqrt{n}$ nodes whose distance from $x_j$ is at most
$\frac{1}{2}\log_{2c}n$. Therefore among $y_j, \dotsc, y_{j+n-1}$, at
least $n - \sqrt{n} - 1 \ge n - 2\sqrt{n}$ are at distance at least
$\frac{1}{2}\log_{2c}n$. In other words,
$\Pr_{\ell \in [n]}[d_{\bar{C}}(x_j, y_{j+\ell-1}) \geq
\frac{1}{2}\log_{2c}n] \ge 1-\frac{2}{\sqrt{n}}$, where $\bar{C}$
denotes the undirected graph induced by $C$ (by removing edge
directions). Therefore there exists a shift $\ell_0 \in [n]$ such that
$\bigl|\{j \in [n] : d_{\bar{C}}(x_j, y_{j+\ell_0-1}) \geq
\frac{1}{2}\log_{2c}n\}\bigr| \geq n-2\sqrt{n} \geq n/2$.

Fixing $\ell_0$, consider the following communication problem. For
each $j \in [n]$, let $s_j=x_j$ and $t_j = y_{j+\ell_0-1}$ be a
source-sink pair, and let $A_j = \{0,1\}$ be the set of possible
messages. The circuit $C$ equipped with $1$-uniform edge capacities is
a network coding solution to this problem with rate $r \ge 1$. By the
undirected $n$-pairs conjecture, there is a multicommodity flow in
$\bar{C}$ that transfers one unit of flow from each source to its
corresponding sink. For every $j$, let $f^j \colon E \to [0,1]$ be the
flow associated with commodity $j$. Then
\begin{equation*}
  |E| = \sum_{e \in E}{c_e} \ge \sum_{e \in E}{\sum_{j \in [n]}{f^j(e)}} \geq \Omega(n \log_c n) \;,
\end{equation*}
which completes the proof of \cref{thm:multiplication:shiftLB}.

\section[A Lower Bound for Depth 3 Boolean Circuits]{A Lower Bound for Depth 3 Boolean Circuits Computing Multiplication}
\label{sec:multiplication:LBdepth3}

\begin{figure}[t]
  \centerfloat
  \includegraphics{multiplication/circuit}
  \caption{The depth $3$ circuit $C$.}
  \label{fig:multiplication:circuit}
\end{figure}

Let $C$ be a depth $3$ circuit that computes multiplication such that
the number of gates in the second layer of $C$ is at most $\eps n$ for
some small $\eps \in (0,1)$ and for every $u \in Y$,
$deg_{\bar{C}[X \cup Y]}(u) \le c$, where once again $\bar{C}$ denotes
the undirected graph induced by $C$, and $\bar{C}[X \cup Y]$ is the
subgraph of $\bar{C}$ induced by $X \cup Y$. By slightly increasing
$c$ and $\eps$ (by a small constant factor) and without loss of
generality, we can assume that this applies for all $u \in X$ as well.
%
To see this, note that by the assumption on $Y$, the total number of
edges in $\bar{C}[X \cup Y]$ is at most $cn$. Therefore there are at
most $n / 10$ nodes in $X$ whose degree in $\bar{C}[X \cup Y]$ is more
than $10c$. For each such node $u \in X$, we can replace all edges
adjacent to $u$ in $\bar{C}[X \cup Y]$ by an extra second-layer node
$f_u$ connected to $u$ and its neighbors in $\bar{C}[X \cup Y]$.
%
Denote the input and output gates of $C$ by
$X = \{x_1, \dotsc, x_n,\hat{x}_1, \dotsc, \hat{x}_n\}$ and
$Y = \{y_1, \dotsc, y_{2n}\}$ respectively, and denote the set of the
middle-layer gates by $F = \{f_1, \dotsc, f_{\eps n}\}$ (see
\cref{fig:multiplication:circuit}).

\begin{figure}[t]
  \centerfloat
  \includegraphics{multiplication/network}
  \caption{Given the $2$-layer circuit $\Gamma$ spanned by
    $x_1, \dotsc, x_n,y_1, \dotsc, y_n$, we construct the
    communication network graph $G$.}
   \label{fig:multiplication:network}
\end{figure}
As before, we focus on computing the shift function, thus limiting the
input to $(\hat{x}_1, \dotsc, \hat{x}_n)$ to have exactly one
$1$-entry. We next partition $(x_1, \dotsc, x_n)$ into consecutive
blocks of size $k = 20$ bits each. For every $\ell \in [n/k]$ let
$B_\ell = \{k(\ell-1)+1, \dotsc, k\ell\}$ be the set of indices
belonging to the $\ell$th block.
\begin{definition}
  For every $\alpha \in [n]$ and $\ell \in [n/k]$, we say $B_\ell$ is
  \emph{far from all targets} (with respect to $\alpha$) if for all
  sources in the block are at distance at least
  $\frac{1}{2}\log_{2c}n$ from all respective destinations in
  $\bar{C}[X \cup Y]$. That is for every $u, v \in B_\ell$,
  $d_{\bar{C}[X \cup Y]}(x_u, y_{v+\alpha-1}) \geq
  \frac{1}{2}\log_{2c}n$.
\end{definition}
Let $\alpha \in_R [n]$. By the constraint on the degrees, for every
$j \in [n]$, there are at most $\sqrt{n}$ nodes whose distance from
$x_j$ is at most $\frac{1}{2}\log_{2c}n$ in $\bar{C}[X \cup
Y]$. Therefore for every $\ell \in [n/k]$,
\begin{equation*}
  \Pr_{\alpha \in_R[n]}\bigl[B_\ell \; \text{is far from all targets}\bigr] \ge 1 - \frac{k^2}{\sqrt{n}} \;.
\end{equation*}
By averaging we get that for large enough $n$ there is some
$\alpha_0 \in [n]$ such that there are at least
$\frac{n}{k} - k \sqrt{n} \geq \frac{9n}{10k}$ blocks which are far
from all targets. Without loss of generality, we may assume for ease
of notation that $\alpha_0 = 1$. By hardwiring $1$ for $\alpha_0$ into
the circuit $C$, the circuit now simply transfers $(x_1, \dotsc, x_n)$
to $(y_1, \dotsc, y_n)$.

\paragraph{Reduction to Network Coding.}
Let $x = (x_1, \dotsc, x_n)$ and $i \in [\eps n]$. By slightly abusing
notation, we denote the value of the gate $f_i$ when evaluating the
circuit by $f_i(x_1, \dotsc, x_n)$.  By averaging, there exist a
string $(\hat{f}_1, \ldots, \hat{f}_{\eps n})$ and a set
$\mathcal{F} \subseteq \{0, 1\}^n$ such that
$|\mathcal{F}| \geq 2^{(1-\eps) n}$ and such that for every
$x = (x_1, \dotsc, x_n) \in \mathcal{F}$ and $i \in [\eps n]$,
$f_i(x_1, \dotsc, x_n) = \hat{f}_i$.  By hardwiring
$(\hat{f}_1, \ldots, \hat{f}_{\eps n})$ for $(f_1, \dotsc, f_n)$ into
the circuit $C$, we get a new circuit denoted $\Gamma$ that contains
only the input and output gates of $C$, and transfers
$(x_1, \dotsc, x_n)$ to $(y_1, \dotsc, y_n)$ for every
$(x_1, \dotsc, x_n) \in \mathcal{F}$. Moreover, the set of edges
between $X$ and $Y$ in $\Gamma$ is equal to the set of edges between
$X$ and $Y$ in $C$.

Next, we construct a communication network $G$ by adding some nodes
and edges to $\Gamma$, as demonstrated also in
\cref{fig:multiplication:network}. We add a new set of nodes
$\{s_j, a_j, t_j\}_{j=1}^{n/k} \cup \{u\}$. For every
$\ell \in [n/k]$, add edges $(s_{\ell}, a_{\ell})$ and $(s_{\ell}, u)$
of capacity $k$ and edges $(u, a_{\ell})$ and $(u, t_{\ell})$ of
capacity $c_{\ell} = \E[|R_\ell|]$, where $R_\ell$ is the message sent
to player $p_\ell$ by the supervisor player in the
$\mathcal{F}$-correction game protocol for $n/k+1$ players guaranteed
in \cref{l:protocolBound}. In addition, for every
$\ell \in [n/k]$ and every $j \in B_{\ell}$ add edges
$(a_{\ell}, x_j)$ and $(y_j, t_{\ell})$ of capacity $1$. All edges of
$\Gamma$ are assigned capacity of $1$.

\paragraph{Transmitting Data.}
In what follows, we will lower bound the communication rate of the
newly constructed network $G$.
\begin{lemma}\label{l:comRate}
  There exists a network coding solution on $G$ that achieves rate $k$.
\end{lemma}
To this end, let $A_1, \dotsc, A_{n/k} \in \{0,1\}^k$ be independent
uniform random variables. We next give a protocol by which the sources
$s_1, \dotsc, s_{n/k}$ transmit $A_1, \dotsc, A_{n/k}$ to the targets
$t_1, \dotsc, t_{n/k}$. The protocol employs as an intermediate step
the correction game protocol guaranteed by
\cref{l:protocolBound}.
\begin{enumerate}
\item For every $\ell \in [n/k]$, $s_\ell$ sends $A_\ell$ to $a_\ell$
  over the edge $(s_\ell, a_\ell)$ and to $u$ over the edge $(s_\ell, u)$.
\item Employing the $\mathcal{F}$-correction game protocol with
  $n/k+1$ players, for every $\ell \in [n/k]$, $u$ sends a message
  $R_\ell$ to $a_\ell$ over the edge $(u, a_\ell)$ and to $t_\ell$ over
  the edge $(u, t_\ell)$. Following the correction game protocol, for
  every $\ell$, given $R_\ell$, $a_\ell$ and $t_\ell$ produce a string
  $\chi_\ell$ satisfying that
  $(A_1\oplus \chi_1)\circ \dotsb \circ(A_{n/k}\oplus \chi_{n/k}) \in
  \mathcal{F}$.
\item For every $\ell \in [n/k]$ and every $i \in [k]$, $a_\ell$
  transmits the $i$th bit of $A_\ell \oplus \chi_\ell$ to the $i$th
  gate in the $\ell$th block, namely $x_{(\ell-1)k+i}$. Note that
  $(x_1, \dotsc, x_n) = (A_1\oplus
  \chi_1)\circ \dotsb \circ(A_{n/k}\oplus \chi_{n/k}) \in \mathcal{F}$.
\item Next, the communication network employs the circuit $\Gamma$ and
  transmits $(x_1, \dotsc, x_n)$ to $(y_1, \dotsc, y_n)$. For every
  $\ell \in [n/k]$ and every $i \in B_\ell$, $y_i$ transmits $x_i$ to
  $t_\ell$.
\item Finally, for every $\ell \in [n/k]$, $t_\ell$ now holds both
  $A_\ell \oplus \chi_\ell$ and $\chi_\ell$. Therefore $t_\ell$ can
  recover $A_\ell$.
\end{enumerate}
By invoking the protocol described above, every one of the $n/k$
sources sends $k$ bits to the corresponding target. For every edge
$e \in G$, let $A_e$ denote the random variable giving the message
sent on the edge $e$ when executing the protocol.
\begin{claim}
  For every $e \in G$, $H(A_e) \leq c_e$.
\end{claim}
\begin{proof}
  First note that for every $\ell \in [n/k]$, every edge $e$ leaving
  $s_\ell$ has capacity $k$ and transmits $A_\ell$. Therefore
  $H(A_\ell) = k \leq c_e$. Every edge $e$ that is not leaving any
  source nor $u$ has capacity $1$ and transmits exactly one bit (not
  necessarily uniformly random) of information. Therefore
  $c_e = 1 \geq H(A_e)$.  Finally, let $e$ be an edge leaving
  $u$. Then there exists some $\ell \in [n/k]$ such that
  $e = (u, a_\ell)$ or $e = (u, t_\ell)$. In both cases the message
  transmitted on $e$ is $R_\ell$ and the capacity $c_e$ of $e$
  satisfies $c_e = c_\ell = \E[|R_\ell|] \geq H(R_\ell)$, where the
  last inequality follows from Shannon's Source Coding theorem, as all
  messages are prefix-free.
\end{proof}
We can therefore conclude that the network $G$ achieves rate $\geq k$,
and the proof of \cref{l:comRate} is complete.

\paragraph{Deriving the Lower Bound.}
By \cref{con:multiplication:undirected}, the underlying undirected
graph $\bar{G}$ achieves a multicommodity-flow rate $\geq k$. Therefore
there exists a multicommodity flow
$\{f^{\ell}\}_{\ell \in [n/k]} \subseteq [0,1]^{E(\bar{G})}$ that
achieves rate $k$. We first observe that at most a constant fraction
of the flow can go through the supervisor node $u$. To see this, we
note that as $|\mathcal{F}| \ge 2^{(1-\eps)n}$, then by
\cref{l:protocolBound}, for small enough (constant) $\eps$, the
expected total information sent by the supervisor in the
$\mathcal{F}$-correction game with $n/k$ players is at most
\begin{equation}
  \label{eq:protocol}
  \frac{3n}{k} + \frac{2n}{k}\lg\Bigl(k\sqrt{\frac{\eps}{2}} + 1\Bigr) + \sqrt{\frac{\eps}{8}} \cdot n \lg\frac{2}{\eps} \leq \frac{5n}{k}
\end{equation}
Therefore by the definition of the capacities
$\{c_\ell\}_{\ell \in [n/k]}$ we get that
\begin{equation}
  \label{eq:capacities}
  \sum_{\ell \in [n/k]}{c_{ua_\ell}} = \sum_{\ell \in [n/k]}{c_{ut_\ell}} = \sum_{\ell \in [n/k]}{c_{\ell}} \leq \frac{5n}{k}
\end{equation}
Since $\{f^{\ell}\}_{\ell \in [n/k]}$ achieves rate $k$ we conclude
that for every $v \in V(\bar{G})$ adjacent to $u$ we have
$k \sum_{\ell \in [n/k]}{(f^{\ell}(u, v) + f^{\ell}(v, u))} \leq
c_{uv}$. Therefore
\begin{align*}
  \label{eq:uCapacity}
  k \cdot\sum_{v \in V(\bar{G}) : (u, v) \in E(\bar{G})}{ \sum_{\ell \in [n/k]}{(f^{\ell}(u,v) + f^{\ell}(v,u))}} &\leq \sum_{v \in V(\bar{G}) : (u, v) \in E(\bar{G})}{c_{uv}}\\
                                                                                                              &= \sum_{\ell \in [n/k]}{c_{us_\ell}} + \sum_{\ell \in [n/k]}{(c_{ua_\ell}+c_{ut_\ell})} \\
                                                                                                              &\leq n + \frac{10n}{k}\;.
\end{align*}
Rearranging we get that
\begin{equation}
  \label{eq:uTotal}
  \sum_{v \in V(\bar{G}) : (u, v) \in E(\bar{G})}{ \sum_{\ell \in [n/k]}{(f^{\ell}(u,v) + f^{\ell}(v,u))}} \leq \frac{n}{k} + \frac{10n}{k^2} \leq 1.5\frac{n}{k} \;.
\end{equation}
By the flow-conservation constraint, we know that the total amount of
flow that can go through $u$ is $\leq 0.75 \frac{n}{k}$. By averaging,
at least a $1/6$ fraction of the sources send at least $1/10$ units of
flow through $\bar{G} - u$. By the choice of $\alpha_0$, in
$\bar{G}-u$, at least a $1/15$ of the sources are at least
$\frac{1}{2}\log_{2c}(n)$ away from their targets. Without loss of
generality, assume these are the first $\frac{n}{15k}$ sources. We
conclude that
\begin{align*}
  cn &\geq |E[X \cup Y]| \\
     &= \sum_{e \in E[X \cup Y]}{c_e} \\
     &\geq k \cdot \sum_{(v, w) \in E[X \cup Y]}{\sum_{\ell \in [n/k]}{f^{\ell}(v,w)+f^{\ell}(w,v)}} \\
     &\geq k \cdot \sum_{\ell \in [n/15k]}{\sum_{(v, w) \in E[X \cup Y]}{f^{\ell}(v,w)+f^{\ell}(w,v)}} \\
     &\geq \frac{n}{30}\log_{2c}(n) \;,
\end{align*}
and therefore $c \geq \Omega\bigl(\frac{\log n}{\log \log n}\bigr)$, and
the proof of \cref{thm:multiplication:LBdepth3} is now complete.

\subsection{Remarks and Extensions}
For sake of fluency, some minor remarks and extensions were
intentionally left out of the text, and will be discussed now.
\paragraph{Circuits with Bounded Average Degree.}
Our results still hold if we relax the second requirement of
\cref{thm:multiplication:LBdepth3} and require instead that the number
of edges in $\bar{C}[X \cup Y]$ is at most $cn$. That is, the average
degree in $\bar{C}[X \cup Y]$ is at most $c$. To see this, note that
under this assumption, there are at most $0.001n$ gates in $X \cup Y$
whose degree in $\bar{C}[X \cup Y]$ is larger than $1000c$. For each
such gate $v$, add a new node $f$ in the middle layer, and connect $v$
and all the neighbors of $v$ in $\bar{C}[X \cup Y]$ to $f$. Then
delete all the edges adjacent to $v$ in $\bar{C}[X \cup Y]$. The
number of nodes added to the middle layer is at most $0.001n$, and the
degree of all nodes in $\bar{C}[X \cup Y]$ is now bounded by
$1000c$. The rest of our proof continues as before.

\paragraph{Shifts vs. Cyclic Shifts.}
In order to prove lower bounds for circuits computing multiplication,
our results are stated in terms of shifts (which are a special case of
products, as mentioned). This is in contrast to Valiant's conjectures,
which are stated in terms of cyclic shifts. However, we draw the
readers attention to the fact that our proofs work for cyclic shifts
as well. The exact same arguments apply, and the proofs remain
unchanged.






%%% Local Variables:
%%% mode: latex
%%% TeX-master: "thesis"
%%% End:
