\begin{filecontents*}{\jobname.xmpdata}
\Title{A Song of Johnson and Lindenstrauss}
\Author{Casper Benjamin Freksen}
\Keywords{
theoretical computer science\sep
algorithms\sep
dimensionality reduction\sep
Johnson-Lindenstrauss\sep
lower bounds\sep
multiplication\sep
circuit lower bounds
}
\Language{en-GB}
\Publisher{Aarhus University}
\Copyright{Copyright \copyright\ 2020 Casper Benjamin Freksen.
This work is licensed under a Creative Commons Attribution 4.0 International Public License (CC-BY-4.0)}
\CopyrightURL{https://creativecommons.org/licenses/by/4.0/legalcode}
\Owner{Casper Benjamin Freksen}
\end{filecontents*}

\documentclass[11pt,a4paper,twoside,openright,british,final]{memoir}
\usepackage[a-3u]{pdfx}
\catcode30=12                   % Workaround for bug in biblatex/pdfx interaction (fixed upstream)

% Page layout stuff
% TODO: Check if AU has requirements, perhaps need wide enough spine margin for printing
\settrims{0pt}{0pt}
% \setlrmarginsandblock{1in}{*}{1.5}
\semiisopage[10]
\OnehalfSpacing                 % TODO: Might be too ugly, perhaps not with Palatino
\checkandfixthelayout

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage[danish, british, UKenglish]{babel}
\usepackage{microtype}
\usepackage[inline]{enumitem}   % Might not be used (might only have been used for commalist)
\usepackage{graphicx}
\graphicspath{{figures/}}
\usepackage{epsfig}
% \usepackage[textsize=small]{todonotes}
\usepackage{rotating}
% \newcommand{\lowtodo}[1]{\todo[color=green]{\footnotesize{\textsc{Low}: #1}}}

\usepackage{stackengine}
\DeclareUnicodeCharacter{1EC5}{\stackon[-5.5pt]{ê}{\~{}}}

\usepackage[natbib, doi, backref, useprefix, citestyle=alphabetic, bibstyle=alphabetic, maxbibnames=20, minbibnames=5]{biblatex}
\DefineBibliographyExtras{british}{\def\finalandcomma{\addcomma}}
\renewcommand*{\mkbibnamegiven}[1]{%
  \ifitemannotation{highlight}
    {\emph{#1}}
    {#1}}
\renewcommand*{\mkbibnamefamily}[1]{%
  \ifitemannotation{highlight}
    {\emph{#1}}
    {#1}}
\addbibresource{bibliography.bib}
% \usepackage{showframe}
\usepackage{newpxtext,newpxmath}
\usepackage{cleveref}

\epigraphfontsize{\small\itshape}
\setlength\epigraphwidth{7cm}
% \epigraphtextposition{flushright}
\setlength\epigraphrule{0pt}

\newcommand*{\confver}[1]{Previously published as \autocite{#1}}
\usepackage{adforn}
\newcommand*{\sectionbreak}{\fancybreak{\adfflourishleftdouble\quad\adfast9\quad\adfflourishrightdouble}}

\newcommand{\R}{\mathbb{R}}
\newcommand{\Rpos}{\mathbb{R}_+}
\newcommand{\Nzero}{\mathbb{N}_0}
\newcommand{\Npos}{\mathbb{N}_1}
\newcommand{\K}{\mathbb{K}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\T}{\transp}
\newcommand{\bigO}{\mathcal{O}}
\newcommand{\eps}{\varepsilon}
\newcommand{\zerovector}{\ensuremath{\mathbf{0}}}
\newcommand{\indicator}{\ensuremath{\vvmathbb{1}}}
\providecommand{\eqdef}{\coloneq}
\newcommand{\In}{\textrm{In}}
\DeclareMathOperator{\supp}{supp}
\DeclareMathOperator{\spn}{span}
\DeclareMathOperator{\tail}{tail}
\newcommand{\dotprod}[3][]{\mathopen#1\langle #2 , #3 \mathclose#1\rangle}
\newcommand{\abs}[2][]{\mathopen#1\lvert #2 \mathclose#1\rvert}
\newcommand{\norm}[3][]{\mathopen#1\lVert #2 \mathclose#1\rVert_#3}
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\median}{median}

\makeatletter
\newtheorem*{rep@theorem}{\rep@title}
\newcommand{\newreptheorem}[2]{%
\newenvironment{rep#1}[1]{%
 \def\rep@title{#2 \ref{##1}}%
 \begin{rep@theorem}}%
 {\end{rep@theorem}}}
\makeatother

\newtheorem{theorem}{Theorem}[chapter]
\newreptheorem{theorem}{Theorem}
\newtheorem{lemma}[theorem]{Lemma}
\newreptheorem{lemma}{Lemma}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{notation}[theorem]{Notation}
\newtheorem{proposition}[theorem]{Proposition}
\newreptheorem{proposition}{Proposition}
\newtheorem{corollary}[theorem]{Corollary}
\newreptheorem{corollary}{Corollary}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{conjecture}[theorem]{Conjecture}
\newreptheorem{conjecture}{Conjecture}

\settocdepth{subsection}
\setsecnumdepth{subsection}

\begin{document}

\title{A Song of Johnson and Lindenstrauss}
\author{Casper Benjamin Freksen}
\date{\today}

\pagenumbering{roman}
\include{cover}

\frontmatter
\include{frontmatter}

\cleardoublepage
\tableofcontents
\cleardoublepage


\mainmatter
\pagenumbering{arabic}

\part{Overview}
\label{part:overview}

\include{introduction}

\include{results}


\part{Publications}
\label{part:publications}

\include{toeplitz}

\include{feature_hashing}

\include{multiplication}

\part{Appendices}
\appendix

\include{appendix/notation}
\include{appendix/intro_proofs}

\cleardoublepage
\backmatter

\printbibliography
\label{cha:bib}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
